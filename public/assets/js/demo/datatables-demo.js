// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable( {
    scrollY:        'auto',
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
    columnDefs: [
        { width: '30%', targets: 0 }
    ],
    fixedColumns: true
} );;
});
