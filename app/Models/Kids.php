<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Nov 2018 07:20:34 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InstaUsersConfig
 * 
 * @property int $config_id
 * @property int $config_user_id
 *
 * @package App\Models
 */
class Kids extends Model
{
	protected $table = 'kids';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'kid_name',
		'kid_dob',
		'kid_gender',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
