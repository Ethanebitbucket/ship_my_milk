<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
	protected $primaryKey = 'order_detail_id';
    public $timestamps = false;

    public function order(){
        $this->belongsTo('App\Models\Order');
    }
}
