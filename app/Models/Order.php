<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $primaryKey = 'order_id';
    public $timestamps = true;

    public function orderdetail(){
        return $this->hasOne('App\Models\OrderDetail','order_id');
    }
}
