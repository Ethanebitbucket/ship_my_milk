<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicePrice extends Model
{
    protected $table = 'regional_service_price';
	protected $primaryKey = 'service_plan_id';
	public $timestamps = true;

	protected $casts = [
		'region_id' => 'int'
	];

	protected $fillable = [
		'service_price',
		'service_id',
		'region_id',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
}
