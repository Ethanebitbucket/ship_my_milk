<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';
	protected $primaryKey = 'region_id';
	public $timestamps = true;

	protected $casts = [
		'region_id' => 'int'
	];

	protected $fillable = [
		'region_name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function box()
    {
        return $this->hasMany('App\Models\BoxPrice');
    }
}
