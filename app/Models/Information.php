<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
	protected $primaryKey = 'info_id';
	public $timestamps = false;
}
