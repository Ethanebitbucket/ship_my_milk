<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';
	protected $primaryKey = 'slider_id';
	public $timestamps = true;

	protected $casts = [
		'region_id' => 'int'
	];

	protected $fillable = [
		'slider_type',
		'slider_image',
		'slider_text',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function scopeOfType($query, $type)
    {
        return $query->where('slider_type', $type);
    }
}
