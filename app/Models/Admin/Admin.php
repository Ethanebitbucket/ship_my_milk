<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin_users';
    protected $fillable = ['firstname','lastname','email','contact','date_of_birth','password','tax_percentage','address','country_code','zip_code','state_code','city','activation_key','admin_role','gender','active'];
     
}
