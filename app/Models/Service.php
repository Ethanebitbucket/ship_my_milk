<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
	protected $primaryKey = 'service_id';
	public $timestamps = true;

	protected $casts = [
		'region_id' => 'int'
	];

	protected $fillable = [
		'service_name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function price()
    {
        return $this->hasMany('App\Models\ServivePrice');
    }
}
