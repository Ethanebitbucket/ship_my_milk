<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $table = 'boxes';
	protected $primaryKey = 'box_id';
	public $timestamps = true;
    protected $appends = ['full_name'];
	protected $casts = [
		'box_id' => 'int'
	];

	protected $fillable = [
		'box_type',
		'box_subtitle',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function price()
    {
        return $this->hasOne('App\Models\BoxPrice');
    }
    public function getFullNameAttribute()
    {
        return " {$this->box_size} {$this->box_subtitle}, {$this->box_type} ";
    }
}
