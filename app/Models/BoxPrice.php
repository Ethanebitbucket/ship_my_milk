<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoxPrice extends Model
{
    protected $table = 'regional_box_price';
	protected $primaryKey = 'box_plan_id';
	public $timestamps = true;

	protected $casts = [
		'region_id' => 'int'
	];

	protected $fillable = [
		'box_price',
		'box_id',
		'region_id',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function box()
    {
        return $this->belongsTo('App\Models\Box','box_id');
    }
    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
}
