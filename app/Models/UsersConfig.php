<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 14 Nov 2018 07:20:34 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InstaUsersConfig
 * 
 * @property int $config_id
 * @property int $config_user_id
 *
 * @package App\Models
 */
class UsersConfig extends Model
{
	protected $table = 'users_config';
	protected $primaryKey = 'config_id';
	public $timestamps = false;

	protected $casts = [
		'config_user_id' => 'int'
	];

	protected $fillable = [
		'config_user_id',
		'config_mute_notification',
		'config_primary_contact',
	];
	
	public function getConfigPrimaryContact($value)
    {
        if(!is_null($value)) {
            return @unserialize($value);
		} 
		return $value;
    }
	
	public function getConfigNotificationSetting($value)
    {
        if(!is_null($value)) {
            return @unserialize($value);
		}
		return $value;
    }
    public function Users()
    {
        return $this->belongsTo('App\User');
    }
}
