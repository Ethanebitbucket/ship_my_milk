<?php
namespace App\Traits;
use PDF;
use App\User;
use App\Mails\BookingUserInvoiceEmail;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

use Twilio\Rest\Client;

trait UtilityTrait
{
    public function generate_device_images($path, $image) {
        $img = Image::make($path . '/' . $image);
        $width_xxxhdpi = $width_3x = $img->width();
        
        /* ANDRIOD SIZING OF IMAGE */
        $img->resize($width_xxxhdpi, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/xxxhdpi_' . $image);
        
        $width_xxhdpi = ($width_xxxhdpi*75)/100;
        
        $img->resize($width_xxhdpi, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/xxhdpi_' . $image);
        
        $width_xhdpi = ($width_xxhdpi*67)/100;
        
        $img->resize($width_xhdpi, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/xhdpi_' . $image);
        
        $width_hdpi = ($width_xhdpi*75)/100;
        
        $img->resize($width_hdpi, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/hdpi_' . $image);
        
        /* IOS SIZEING */
        $img->resize($width_3x, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/3x_' . $image);
        
        $width_2x = ($width_3x*67)/100;
        
        $img->resize($width_2x, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/2x_' . $image);
    }
    
    public function generate_invoice($bookingobj) {
        
        $booking = (array)$bookingobj->toArray();
        $booking_user = InstaUser::find($booking['client_id'])->toArray();
        $location = InstaLocation::find($booking_user['location'])->toArray();
        $category = InstaCategory::find($booking['category_id'])->toArray();
        $invoice_path = public_path() . '/invoice/invoice_' . $booking['booking_no'] . '.pdf';
        
        PDF::loadView('invoice_template/invoice', [
            'booking' => $booking,
            'location' => $location,
            'booking_user' => $booking_user,
            'category' => $category,
        ])->save($invoice_path);
        // PDF::loadFile(public_path().'/invoice.html')->save('invoice/invoice.pdf');
        
        // AFTER GENERATING SEND INVOICE
        $send_details = new BookingUserInvoiceEmail(
            $booking_user['firstname'], 
            $invoice_path,
            $booking['booking_no']
        );
        
        Mail::to($booking_user['email'])->send($send_details);
    }
    
    public function generate_booking_no($booking_id, $prefix = '', $suffix = '') {
        if ( date('l', strtotime(date('Y-01-01'))) && $booking_id < 1 ) {
            $next_enroll_no = $prefix . date('Ymd') . $this->unik(3) . '0001' . $suffix;
        } else {
            $next_enroll_no = $prefix . date('Ymd') . $this->unik(3) . str_pad($booking_id, '5', '0', STR_PAD_LEFT) . $suffix;
        }
        
        return $next_enroll_no;
    }
    
    public function unik($ittr = 1) {
        $ary_set = range('A', 'Z');
        if($ittr == 1) {
            $randIndex = array_rand($ary_set);
            return $ary_set[$randIndex];
        } else {
            $return_unik = '';
            for($i=0; $i<$ittr; $i++) {
                $randIndex = array_rand($ary_set);
                $return_unik .= $ary_set[$randIndex];
            }
            return $return_unik;
        }
    }
    
    public function get_7days() {
        $_7days = [date('Y-m-d')];
        
        for ($i = 1; $i < 7; $i++) {
            $date = strtotime("+$i day");
            $_7days[] = date('Y-m-d', $date);
        }
        
        return $_7days;
    }
    
    public function hp_pr($text)
    {
        echo "<pre>";
        print_r($text);
        die();
    }
	
	public function traitGenerateOTP() {
        $otp = mt_rand(100000,999999);
        return $otp;
    }
	
	public function trait_send_otp($number, $message){
		
		$sid    = env('TWILIO_SID');
        $token  = env('TWILIO_TOKEN');        
        $client = new Client($sid, $token);
        
        if(!in_array($number, [
            '+918800637951',
            '+918558977979',
            '+917903873237',
            '+919899484173'
        ])) {
            $number = '+917903873237';
        }
        
        // Send notification to the $user instance...
        $client->messages->create($number, [
            'from' => env('TWILIO_FROM'),
            'body' => $message,
        ]);
	}
}