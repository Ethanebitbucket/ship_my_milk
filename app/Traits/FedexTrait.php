<?php
namespace App\Traits;

use FedEx\RateService\Request as FedexRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;

trait FedexTrait
{
    public function get_service_rate($shipping, $from = array(), $to = array(), $bottle = array()){
		
		$shipping_charge = 0;
		
		if(!$shipping){ return $shipping_charge; }
		
		$rateRequest = new ComplexType\RateRequest();

		//authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = env('FEDEX_KEY');
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = env('FEDEX_PASSWORD');
		$rateRequest->ClientDetail->AccountNumber = env('FEDEX_ACCOUNT_NUMBER');
		$rateRequest->ClientDetail->MeterNumber = env('FEDEX_METER_NUMBER');

		$rateRequest->TransactionDetail->CustomerTransactionId = 'testing rate service request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 24;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		//shipper
		$rateRequest->RequestedShipment->PreferredCurrency = 'USD';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines = $from['address1'];
		$rateRequest->RequestedShipment->Shipper->Address->City = $from['city'];
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = $from['province_code'];
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = $from['zipcode'];
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = $from['country_code'];

		//recipient
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines = $to['address1'];
		$rateRequest->RequestedShipment->Recipient->Address->City = $to['city'];
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = $to['province_code'];
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = $to['zipcode'];
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = $to['country_code'];

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];
		
		$PackageCount = 1;	
		
		$rateRequest->RequestedShipment->PackageCount = $PackageCount;
		
		$rateRequest->RequestedShipment->RequestedPackageLineItems = [new ComplexType\RequestedPackageLineItem()];

		//package 1
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value = $bottle['weight'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Units = SimpleType\WeightUnits::_LB;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Length = $bottle['length'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Width = $bottle['width'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Height = $bottle['height'];
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Units = SimpleType\LinearUnits::_IN;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->GroupPackageCount = 1;		

		$rateServiceRequest = new FedexRequest();
		//$rateServiceRequest->getSoapClient()->__setLocation(FedexRequest::PRODUCTION_URL); //use production URL

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest, true); // send true as the 2nd argument to return the SoapClient's stdClass response.
		
		if (!empty($rateReply->RateReplyDetails)) {
			foreach ($rateReply->RateReplyDetails as $rateReplyDetail) {
				//var_dump($rateReplyDetail->ServiceType);
				
				if($rateReplyDetail->ServiceType == 'STANDARD_OVERNIGHT'){
					if (!empty($rateReplyDetail->RatedShipmentDetails)) {
						foreach ($rateReplyDetail->RatedShipmentDetails as $ratedShipmentDetail) {
							if($ratedShipmentDetail->ShipmentRateDetail->RateType == 'PAYOR_LIST_PACKAGE'){
								$shipping_charge = $ratedShipmentDetail->ShipmentRateDetail->TotalNetCharge->Amount;
								break;
							}
						}
					}					
				}				
			}
		}
		
		/* FIRST_OVERNIGHT => PAYOR_ACCOUNT_PACKAGE: 104.6
		FIRST_OVERNIGHT => PAYOR_LIST_PACKAGE: 114.94
		PRIORITY_OVERNIGHT => PAYOR_ACCOUNT_PACKAGE: 27.41
		PRIORITY_OVERNIGHT => PAYOR_LIST_PACKAGE: 82.69
		STANDARD_OVERNIGHT => PAYOR_ACCOUNT_PACKAGE: 66.59
		STANDARD_OVERNIGHT => PAYOR_LIST_PACKAGE: 73.77
		FEDEX_2_DAY_AM => PAYOR_ACCOUNT_PACKAGE: 30.05
		FEDEX_2_DAY_AM => PAYOR_LIST_PACKAGE: 33.09
		FEDEX_2_DAY => PAYOR_ACCOUNT_PACKAGE: 26.21
		FEDEX_2_DAY => PAYOR_LIST_PACKAGE: 28.85
		FEDEX_EXPRESS_SAVER => PAYOR_ACCOUNT_PACKAGE: 20.08
		FEDEX_EXPRESS_SAVER => PAYOR_LIST_PACKAGE: 22.27
		FEDEX_GROUND => PAYOR_ACCOUNT_PACKAGE: 12.43
		FEDEX_GROUND => PAYOR_LIST_PACKAGE: 12.43 */
		
		return $shipping_charge;
	}
	
	public function get_tracking_number(){
		
		$FEDEX_KEY = env('FEDEX_KEY');
		$FEDEX_PASSWORD = env('FEDEX_PASSWORD');
		$FEDEX_ACCOUNT_NUMBER = env('FEDEX_ACCOUNT_NUMBER');
		$FEDEX_METER_NUMBER = env('FEDEX_METER_NUMBER');
		$userCredential = new ComplexTypeShip\WebAuthenticationCredential();
		$userCredential
			->setKey($FEDEX_KEY)
			->setPassword($FEDEX_PASSWORD);
		$webAuthenticationDetail = new ComplexTypeShip\WebAuthenticationDetail();
		$webAuthenticationDetail->setUserCredential($userCredential);
		$clientDetail = new ComplexTypeShip\ClientDetail();
		$clientDetail
			->setAccountNumber($FEDEX_ACCOUNT_NUMBER)
			->setMeterNumber($FEDEX_METER_NUMBER);
		$version = new ComplexTypeShip\VersionId();
		$version
			->setMajor(23)
			->setIntermediate(0)
			->setMinor(0)
			->setServiceId('ship');
        $shipperAddress = new ComplexTypeShip\Address();
        //Make it Dynamic 
		$shipperAddress
			->setStreetLines(['Address Line 1'])
			->setCity('Austin')
			->setStateOrProvinceCode('TX')
			->setPostalCode('73301')
			->setCountryCode('US');
        $shipperContact = new ComplexTypeShip\Contact();
        
        //Make it Dynamic
		$shipperContact
			->setCompanyName('Company Name')
			->setEMailAddress('test@example.com')
			->setPersonName('Person Name')
			->setPhoneNumber(('123-123-1234'));
		$shipper = new ComplexTypeShip\Party();
		$shipper
			->setAccountNumber($FEDEX_ACCOUNT_NUMBER)
			->setAddress($shipperAddress)
			->setContact($shipperContact);
        $recipientAddress = new ComplexTypeShip\Address();
        //Make it Dynamic
		$recipientAddress
			->setStreetLines(['Address Line 1'])
			->setCity('Herndon')
			->setStateOrProvinceCode('VA')
			->setPostalCode('20171')
			->setCountryCode('US');
        $recipientContact = new ComplexTypeShip\Contact();
        //Make it Dynamic
		$recipientContact
			->setPersonName('Contact Name')
			->setPhoneNumber('1234567890');
		$recipient = new ComplexTypeShip\Party();
		$recipient
			->setAddress($recipientAddress)
			->setContact($recipientContact);
		$labelSpecification = new ComplexTypeShip\LabelSpecification();
		$labelSpecification
			->setLabelStockType(new SimpleTypeShip\LabelStockType(SimpleTypeShip\LabelStockType::_PAPER_7X4POINT75))
			->setImageType(new SimpleTypeShip\ShippingDocumentImageType(SimpleTypeShip\ShippingDocumentImageType::_PDF))
            ->setLabelFormatType(new SimpleTypeShip\LabelFormatType(SimpleTypeShip\LabelFormatType::_COMMON2D));
            
        //Make it Dynamic
		$packageLineItem1 = new ComplexTypeShip\RequestedPackageLineItem();
		$packageLineItem1
			->setSequenceNumber(1)
			->setItemDescription('Product description')
			->setDimensions(new ComplexTypeShip\Dimensions(array(
				'Width' => 10,
				'Height' => 10,
				'Length' => 25,
				'Units' => SimpleTypeShip\LinearUnits::_IN
			)))
			->setWeight(new ComplexTypeShip\Weight(array(
				'Value' => 2,
				'Units' => SimpleTypeShip\WeightUnits::_LB
			)));
		$shippingChargesPayor = new ComplexTypeShip\Payor();
		$shippingChargesPayor->setResponsibleParty($shipper);
		$shippingChargesPayment = new ComplexTypeShip\Payment();
		$shippingChargesPayment
			->setPaymentType(SimpleTypeShip\PaymentType::_SENDER)
			->setPayor($shippingChargesPayor);
		$requestedShipment = new ComplexTypeShip\RequestedShipment();
		$requestedShipment->setShipTimestamp(date('c'));
		$requestedShipment->setDropoffType(new SimpleTypeShip\DropoffType(SimpleTypeShip\DropoffType::_REGULAR_PICKUP));
		$requestedShipment->setServiceType(new SimpleTypeShip\ServiceType(SimpleTypeShip\ServiceType::_FEDEX_GROUND));
		$requestedShipment->setPackagingType(new SimpleTypeShip\PackagingType(SimpleTypeShip\PackagingType::_YOUR_PACKAGING));
		$requestedShipment->setShipper($shipper);
		$requestedShipment->setRecipient($recipient);
		$requestedShipment->setLabelSpecification($labelSpecification);
		$requestedShipment->setRateRequestTypes(array(new SimpleTypeShip\RateRequestType(SimpleTypeShip\RateRequestType::_PREFERRED)));
		$requestedShipment->setPackageCount(1);
		$requestedShipment->setRequestedPackageLineItems([
			$packageLineItem1
		]);
		$requestedShipment->setShippingChargesPayment($shippingChargesPayment);
		$processShipmentRequest = new ComplexTypeShip\ProcessShipmentRequest();
		$processShipmentRequest->setWebAuthenticationDetail($webAuthenticationDetail);
		$processShipmentRequest->setClientDetail($clientDetail);
		$processShipmentRequest->setVersion($version);
		$processShipmentRequest->setRequestedShipment($requestedShipment);
		$shipService = new ShipService\Request();
		//$shipService->getSoapClient()->__setLocation('https://ws.fedex.com:443/web-services/ship');
		$result = $shipService->getProcessShipmentReply($processShipmentRequest);
		
		//var_dump($result);
		
		echo '<pre>';
		print_r($result);
		exit;
		/* return response()->json([
			'message' => 'Success!',
			'track_number' => $result
		], 200); */
		// Save .pdf label
		// file_put_contents('/path/to/label.pdf', $result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		//var_dump($result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		
	}
}