<?php
namespace App\Libraries;
use Illuminate\Support\ServiceProvider;
/* use Sentinel;
use WpPassword; */
use App\Libraries\Hash;
class WpAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $hasher = function($value)
        {
            return Hash::make($value);
        };
        
        $checker = function($value, $hashedValue)
        {
            return Hash::check($value, $hashedValue);
        };
    }
}