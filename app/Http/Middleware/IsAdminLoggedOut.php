<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

use Closure;

class IsAdminLoggedOut
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $admin = $request->session()->get(Config::get('constants.admin_session_key'));
        
        if (!$admin) {
            return $next($request);
        }
        
        return redirect('home/dashboard');
        
    }
}
