<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        
        $admin = $request->session()->get(Config::get('constants.admin_session_key'));
        
        if(isset($admin) && !empty($admin)){
            
            View::share('logged_user', $admin);
            
            $current_route = $request->route()->getAction()['controller'];
            
            if($admin->admin_role == 1){
                
                return $next($request);
                
            }
            
            if($admin->admin_role == 2){
                
                if(preg_match('/\bSettingsController\b/', $current_route)) {
                    
                    return response()->view('admin.error.unauthorized', [
                        'error' => 'Unauthorized'
                    ], 401);
                    
                }
                
                return $next($request);
                
            }
               
        }
        
        return redirect('admin/login');
    
    }
}
