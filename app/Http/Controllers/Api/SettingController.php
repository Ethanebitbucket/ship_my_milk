<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\UsersConfig;

class SettingController extends Controller
{
    
    public function CommunitcationMethod(Request $request,UsersConfig $usersConfig)
    {
        $validator = Validator::make($request->all(), [
            'method_type' => 'required|in:0,1',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if($request->user()->id){
                if(UsersConfig::updateOrCreate(['config_user_id'=>$request->user()->id],['config_primary_contact'=>$request->method_type])){
                    return response()->json([
                        'message' => __("user.method_success"),
                    ], 200);
                }
            }else{
                return response()->json([
                    'message' => __("user.method_error")
                ], 422);
            }
        }
    }

    public function SwitchNotification(Request $request,UsersConfig $usersConfig)
    {
        $validator = Validator::make($request->all(), [
            'notification_flag' => 'required|in:0,1',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if($request->user()->id){
                if(UsersConfig::updateOrCreate(['config_user_id'=>$request->user()->id],['config_mute_notification'=>$request->notification_flag])){
                    return response()->json([
                        'message' => __("user.notification_success"),
                    ], 200);
                }else{
                    return response()->json([
                        'message' => __("user.error_msg"),
                    ], 422);
                }
            }else{
                return response()->json([
                    'message' => __("user.notification_error")
                ], 422);
            }
        }
    }
}
