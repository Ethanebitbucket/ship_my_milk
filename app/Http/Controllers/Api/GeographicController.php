<?php

namespace App\Http\Controllers\api;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Filesystem\FilesystemManager; 
use App\Models\Country;

class GeographicController extends Controller
{
    public function genearteJsonFile(Country $country)
    {
        $geographicArry=array();
        $cindx=0;
        $sindx=0;
        $ctindex=0;
        /* foreach(Country::all() as $country){
            if($country->states->count()){
                foreach($country->states as $state){
                    if($state->cities->count()){
                        $geographicArry[$cindx]['cid']=$country->id;
                        $geographicArry[$cindx]['cname']=$country->name;
                        $geographicArry[$cindx]['state'][$sindx]['state_id']=$state->id;
                        $geographicArry[$cindx]['state'][$sindx]['state_name']=$state->name;
                        foreach($state->cities as $city){
                            $geographicArry[$cindx]['state'][$sindx]['city'][$ctindex]['city_id']= $city->id;
                            $geographicArry[$cindx]['state'][$sindx]['city'][$ctindex]['city_name']= $city->name;
                            $ctindex +=1;
                        }
                        $sindx +=1;
                    }
                }
                $cindx +=1;
            }
        } */
		$local_c = array();
		foreach(Country::all() as $country){
            $local_state = array();
			foreach($country->states as $state){
				$local_city = array();
				foreach($state->cities as $city){
					$local_city[] = array(
						'city_id' => $city->id,
						'city_name' => $city->name,
					);
					
				}
				
				if($local_city){
					$local_state[] = array(
						'state_id' => $state->id,
						'state_name' => $state->name,
						'city' => $local_city
					);	
				}				
			}
            if($local_state){
				$local_c[] = array(
					'cid' => $country->id,
					'cname' => $country->name,
					'state' => $local_state
				);
			}
        }
		
        //echo json_encode($local_c,  JSON_UNESCAPED_UNICODE);exit;
        //echo "<pre>"; print_r($local_c);exit;
        Storage::disk('json')->put('json/geolocations.json', json_encode($local_c, JSON_UNESCAPED_UNICODE));
        return response()->json([
            'message' => __("json.success"),
        ], 200);
    }
    public function shareApiGeoJsonFile(Request $request)
    {
        return response()->json([
            'message' => __("json.data"),
            'geo_json_url' => asset('storage/json/geolocations.json'),
            'geo_json' => Storage::disk('json')->get('json/geolocations.json')
        ], 200);
    }
}
