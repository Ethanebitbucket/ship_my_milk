<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Validator;
use App\Models\Service;
use App\Models\Region;
use App\Models\Box;
use App\Models\BoxPrice;
use App\Models\ServicePrice;
use App\Models\Rating;
class OrderController extends Controller
{
    public function saveOrder(Request $request, Service $service, Box $box, BoxPrice $boxprice, ServicePrice $serviceprice, OrderDetail $orderDetail, Order $order, Region $region) {
        $validator = Validator::make($request->all(), [
                'service_id' => 'required|digits_between:1,3',
                'region_id' => 'required|digits_between:1,3',
                'box_details' => 'array|min:1|max:25',
                'from_address' => 'nullable|array',
                'to_address' => 'required|array',
                'boxes_price' => 'required|json|min:3|max:100',
                'total' => 'required|string|min:1|max:6',
                'service_price' => 'required|string|min:1|max:6',
                'special_msg' => 'nullable|string|min:2|max:100',
                'check_in_date' => 'required|string|min:5|max:20',
                'check_out_date' => 'required|string|min:5|max:20',
                'transaction_id' => 'required|string|min:5|max:20',
                'currency_code' => 'required|string|min:2|max:10',
                'payment_status' => 'required|string|min:5|max:100',
                'payment_amount' => 'required|string|min:2|max:7',
                'country_code' => 'required|string|min:2|max:4',
                'alternate_number' => 'required|digits_between:5,14',
                'payment_mode' => 'required|string|min:6|max:20',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if (Service::where('service_id', $request->service_id)->exists()) {
                $order->service_id = $request->service_id;
            } else {
                return response()->json([
                        'message' => __("home.service_not_exists")
                        ], 422);
            }
            if (Region::where('region_id', $request->region_id)->exists()) {
                $order->region_id = $request->region_id;
            } else {
                return response()->json([
                        'message' => __("home.region_not_exists")
                        ], 422);
            }
            $order->box_details = serialize($request->box_details);
            $order->box_price = serialize(json_decode($request->boxes_price, true));
            $order->check_in_date = $request->check_in_date;
            $order->check_out_date = $request->check_out_date;
            $order->user_id = $request->user()->id;
            if ($request->special_msg) {
                $order->special_msg = $request->special_msg;
            }
            if ($order->save()) {
                $orderDetail->order_id = $order->order_id;
                $orderDetail->transaction_id = $request->transaction_id;
                //this will update dynamically from fedex tracking number
                $orderDetail->tracking_no = rand(100000000000000,999999999999999);
                if(($request->from_address) AND count($request->from_address)>0){
                    $orderDetail->from_address = serialize($request->from_address);
                }
                $orderDetail->to_address = serialize($request->to_address);
                $orderDetail->status = $request->payment_status;
                $orderDetail->amount = $request->payment_amount;
                $orderDetail->currency = $request->currency_code;
                $orderDetail->country_code = $request->country_code;
                $orderDetail->alternate_number = $request->alternate_number;
                $orderDetail->payment_mode = $request->payment_mode;
                if ($orderDetail->save()) {
                    return response()->json([
                            'message' => __("home.order_detail_save_msg"),
                            'order_id'=>$order->order_id
                            ], 200);
                } else {

                    return response()->json([
                            'message' => __("home.order_detail_not_save_msg")
                            ], 422);
                }
            } else {
                return response()->json([
                        'message' => __("home.order_not_save_msg")
                        ], 422);
            }
        }
    }
    public function OrderHistory(Request $request,OrderDetail $orderDetail, Order $order,Region $region,Box $box,Service $service)
    {
        if($request->user()->id){
            $orderhistory = array();
            if($request->user()->orders->count()){
                foreach ($request->user()->orders as $key => $orderdetail) {
                    $box_type=unserialize($orderdetail->box_details);
                    $from_address=unserialize($orderdetail->orderdetail->from_address);
                    $to_address=unserialize($orderdetail->orderdetail->to_address);
                    $orderhistory[$key]['tracking_no']=$orderdetail->orderdetail->tracking_no;
                    $orderhistory[$key]['order_type']=Region::find($orderdetail->region_id)->region_name;
                    $orderhistory[$key]['region_type']=$orderdetail->region_id;
                    $product='';
                    foreach($box_type as $indx=>$value){
                        if($indx!=0){
                            $product.=' \n';
                        }
                        $product.=implode(',',Box::find([$box_type[$indx]['box_id']])->pluck('full_name')->toArray()).'X '.$box_type[$indx]['qty'];
                    }
                    $orderhistory[$key]['product']=$product;
                    $orderhistory[$key]['qty']=implode(',',array_column($box_type,'qty'));
                    if(isset($orderdetail->service_id) and $orderdetail->service_id!=1){
                        $orderhistory[$key]['origin_address']=$from_address['address1'].' '.$from_address['address2'].','.$from_address['city'].','.$from_address['state'].','.$from_address['country'].','.$from_address['zipcode'];
                    }
                    $orderhistory[$key]['destination_address']=$to_address['address1'].' '.$to_address['address2'].','.$to_address['city'].','.$to_address['state'].','.$to_address['country'].','.$to_address['zipcode'];
                    $orderhistory[$key]['check_in_date']=date('d/m/Y',strtotime($orderdetail->check_in_date));
                    $orderhistory[$key]['check_out_date']=date('d/m/Y',strtotime($orderdetail->check_out_date));
                    $orderhistory[$key]['order_placed']=date('d/m/Y',strtotime($orderdetail->created_at));
                    $orderhistory[$key]['service_type']=implode(',',Service::where('service_id',$orderdetail->service_id)->pluck('service_name')->toArray());
                    $orderhistory[$key]['order_id']=$orderdetail->order_id;
                    $orderhistory[$key]['payment_method']=$orderdetail->orderdetail->payment_mode?ucfirst($orderdetail->orderdetail->payment_mode):'Paypal';
                    $orderhistory[$key]['order_status']=$orderdetail->orderdetail->order_status;
                    $orderhistory[$key]['delivery_date']=$orderdetail->orderdetail->	delivery_date;
                }
                return response()->json([
                    'message' => __("order.order_history"),
                    'data'=>$orderhistory
                ], 200,[],JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
            }else{
                return response()->json([
                    'message' => __("order.no_order_exist"),
                    'data'=>$orderhistory
                ], 200);
            }
        }else{
            return response()->json([
                'message' => __("order.user_not_exist_msg")
            ], 422);
        }
    }
    public function checkOrderStatus(Request $request,OrderDetail $orderDetail, Order $order)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer',
            'index' => 'required|integer',
        ]);
        //here will update dynamically from fedex tracking number
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if(Order::where('order_id',$request->order_id)->exists()){
                // this function check delievery status from fedex  
                $orderhistory['order_status']=Order::find($request->order_id)->orderdetail->order_status;
                $orderhistory['order_id']=$request->order_id;
                $orderhistory['index']=$request->index;
                return response()->json([
                    'message' => __("order.order_status"),
                    'data'=>$orderhistory
                ], 200);
            }else{
                return response()->json([
                    'message' => __("order.no_order_exist")
                ], 422);
            }
        }
    }
    public function OrderRating(Request $request,Rating $rating,Order $order)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|integer',
            'rating' => 'required',
            'comment' => 'nullable|string|min:1|max:200',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if(Order::where('order_id',$request->order_id)->exists()){
                if(Rating::create(['order_id' => $request->order_id,'rating' => $request->rating,'comment' => $request->comment])){
                    return response()->json([
                        'message' => __("order.rating_msg_success")
                    ], 200);
                }else{
                    return response()->json([
                        'message' => __("order.rating_msg_error")
                    ], 422);
                }
            }else{
                return response()->json([
                    'message' => __("order.no_order_exist")
                ], 422);
            }
        }
    }
}
