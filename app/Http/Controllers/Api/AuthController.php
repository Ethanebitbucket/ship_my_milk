<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Models\UsersConfig;
use App\Models\Kids;
use App\Traits\UtilityTrait;
use App\Mails\UserVerificationEmail;
use App\Mails\UserPasswordEmail;
use App\Libraries\WpHash;
use App\Http\Controllers\Controller;
use Image;

class AuthController extends Controller
{
	use UtilityTrait;
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
       $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|min:2|max:60',
            'lastname' => 'required|string|min:2|max:60',
            'company_name' => 'nullable|string|min:2|max:255',
            'email' => 'required|string|email|unique:users,email',
            'country_code' => 'required',
            'contact_no' => 'required|unique:users,contact_no',
            'password' => 'required|string|same:confirm_password',
			'user_avatar' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

		if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }

		$wp_hasher = new WpHash(8, true);

        $key = mt_rand(10000000, 99999999); //--> OUR LOGIC
        $user_activation_key = time() . ':' . $wp_hasher->HashPassword($key); //--> WP LOGIC WITH NEW LOGIC KEY

		$user = array(
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'company_name' => $request->company_name,
            'email' => $request->email,
			'user_avatar' => '',
			'country_code' => $request->country_code,
            'contact_no' => $request->contact_no,
            'password' => bcrypt($request->password),
			'activation_key' => $user_activation_key,
        );

		if(!empty($_FILES['user_avatar'])) {
            $imageName = @md5(mt_rand() . time()) . '.' . $request->user_avatar->getClientOriginalExtension();
            $request->user_avatar->move(public_path('avatar'), $imageName);
            $user['user_avatar'] = $imageName;

            //$this->generate_device_images(public_path('avatar'), $imageName);
        } else {
            $user['user_avatar'] = '';
        }

		$user = new User($user);
        $user->save();
        $user_id = $user->id;

		// CONFIGURATION SETUP HERE
        $config = UsersConfig::firstOrNew([
            'config_user_id' => $user_id
        ]);

        $config->save();

		/* $kids = array(
			array(
				'kid_name' => 'Kid 1',
				'kid_dob' => date('Y-m-d H:i:s'),
				'kid_gender' => 'Male'
			),
			array(
				'kid_name' => 'Kid 2',
				'kid_dob' => date('Y-m-d H:i:s'),
				'kid_gender' => 'Female'
			),
		); */

		$kids = array();

		if(isset($request->kids)){
			$kids = $request->kids;
		}

		if(!empty($kids)){
			$kid_data = array();
			foreach($kids as $kid){
				if($kid['kid_name'] != ''){
					$kid_data = array(
						'user_id' => $user_id,
						'kid_name' => $kid['kid_name'],
						'kid_dob' => $kid['kid_dob'],
						'kid_gender' => $kid['kid_gender'],
					);
					$kid = new Kids($kid_data);
					$kid->save();
				}
			}
		}

        // MAILING SETUP
        $url = url('/api/auth/signup/activate?token=' . $user->activation_key);
        Mail::to($user->email)->send(new UserVerificationEmail($user->firstname, $url));

        return response()->json([
            'message' => 'Please verify your account, Link has been sent on your email address.',
            'user_id' => $user_id
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
            'device_type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first()
            ], 422);
        }

		$credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Email or password is wrong.'
            ], 401);

		$user = $request->user();

		if(empty($user)) {
            return response()->json([
                'message' => 'User does not exists!'
            ], 422);
        }

		if(!$user->active) {
            return response()->json([
                'message' => 'Account is not active right now, check your verification email for account activation'
            ], 422);
        }

		$user_id = $user->id;

		$tokenResult = $user->createToken('Personal Access Token');
		$token = $tokenResult->token;

		if (isset($request->remember_me)) {
            $token->expires_at = Carbon::now()->addWeeks(1);
		}

        $token->save();

		if($request->device_type) {
            $user->device_token = ($request->device_token != '') ? $request->device_token : $user->device_token;
            $user->device_type = $request->device_type;
            $user->save();
        }

        $user['user_config'] = $user->UserConfig;
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
			'user_data' => $user
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $user_id = $request->user()->id;
        $user = User::where('id', $user_id)->first();
        $user->device_token = "";
        $user->save();
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ], 200);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

	public function unathorised()
    {
        return response()->json([
            'message' => 'Unauthorized',
        ], 401);
    }

	// THIS IS WEB REQUEST
    public function signupActivate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            $data = array();
            $data['error'] = $validator->errors()->first();
            return view('auth.verification', $data);
        }

        if($request->token) {
            $token = $request->token;
        }

        $user = User::where('activation_key', 'LIKE', $token)->first();

        if (empty($user)) {
            $data = array();
            $data['error'] = 'This activation token is either invalid or expired';
            return view('auth.verification', $data);
        } else {
            $user->active = true;
            $user->activation_key = '';
            $user->email_verified_at = Carbon::now()->today();
            $user->save();

            $data = array();
            $data['name'] = $user->firstname . ' ' . $user->lastname;
            return view('auth.verification', $data);
        }
    }

    public function emailReactivate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $data = array();
            $data['error'] = $validator->errors()->first();
            return view('auth.verification', $data);
        }

        if($request->token) {
            $token = $request->token;
        }

        $user = User::where('activation_key', 'LIKE', $token)->first();

        if (empty($user)) {
            $data = array();
            $data['error'] = 'This activation token is either invalid or expired';
            return view('auth.verification', $data);
        } else {
            $user->active = true;
            $user->activation_key = '';
            $user->email_verified_at = Carbon::now()->today();
            $user->email=$request->email;
            $user->save();

            $data = array();
            $data['name'] = $user->firstname . ' ' . $user->lastname;
            return view('auth.verification', $data);
        }
    }
    
    
	public function request_new_password(Request $request)
	{
	    $validator = Validator::make($request->all(), [
	        'email' => 'required|string|email'
	    ]);

	    if ($validator->fails()) {
	        return response()->json([
	            'message' => $validator->errors()->first()
	        ], 422);
	    }

		$email = $request->email;

	    $user = User::select('*')->where('email', $email)->first();
		//echo $user->firstname;
		//echo '<pre>';print_r($user);exit;

	    if (!$user){
			return response()->json([
				'message' => "User does not exists!"
			], 422);
		}

		if($user) {
			if($user->active == 0) {
				return response()->json([
					'message' => "Your account is not active. Please contact our support staff"
				], 422);
			}
		}

		$password = mt_rand(100000000, 999999999);
		$password_hashed = bcrypt($password);
		$user->password = $password_hashed;
		$user->save();

		Mail::to($email)->send(new UserPasswordEmail($user->firstname, $password));

		return response()->json([
			'message' => 'Password has been sent to your registered email id.'
		], 201);
	}
}
