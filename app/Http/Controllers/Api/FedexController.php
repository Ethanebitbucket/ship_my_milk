<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use FedEx\RateService\Request as FedexRequest;
use FedEx\RateService\ComplexType;
use FedEx\RateService\SimpleType;

use FedEx\ShipService;
use FedEx\ShipService\ComplexType as ComplexTypeShip;
use FedEx\ShipService\SimpleType as SimpleTypeShip;

class FedexController extends Controller {
	
	public $FEDEX_KEY, $FEDEX_PASSWORD, $FEDEX_ACCOUNT_NUMBER, $FEDEX_METER_NUMBER;
	
	public function __construct(){
		
	}
	
    public function get_service_rate(){
		
		$rateRequest = new ComplexType\RateRequest();

		//authentication & client details
		$rateRequest->WebAuthenticationDetail->UserCredential->Key = env('FEDEX_KEY');
		$rateRequest->WebAuthenticationDetail->UserCredential->Password = env('FEDEX_PASSWORD');
		$rateRequest->ClientDetail->AccountNumber = env('FEDEX_ACCOUNT_NUMBER');
		$rateRequest->ClientDetail->MeterNumber = env('FEDEX_METER_NUMBER');

		$rateRequest->TransactionDetail->CustomerTransactionId = 'testing rate service request';

		//version
		$rateRequest->Version->ServiceId = 'crs';
		$rateRequest->Version->Major = 24;
		$rateRequest->Version->Minor = 0;
		$rateRequest->Version->Intermediate = 0;

		$rateRequest->ReturnTransitAndCommit = true;

		//shipper
		$rateRequest->RequestedShipment->PreferredCurrency = 'USD';
		$rateRequest->RequestedShipment->Shipper->Address->StreetLines = ['10 Fed Ex Pkwy'];
		$rateRequest->RequestedShipment->Shipper->Address->City = 'Memphis';
		$rateRequest->RequestedShipment->Shipper->Address->StateOrProvinceCode = 'TN';
		$rateRequest->RequestedShipment->Shipper->Address->PostalCode = 38115;
		$rateRequest->RequestedShipment->Shipper->Address->CountryCode = 'US';

		//recipient
		$rateRequest->RequestedShipment->Recipient->Address->StreetLines = ['13450 Farmcrest Ct'];
		$rateRequest->RequestedShipment->Recipient->Address->City = 'Herndon';
		$rateRequest->RequestedShipment->Recipient->Address->StateOrProvinceCode = 'VA';
		$rateRequest->RequestedShipment->Recipient->Address->PostalCode = 20171;
		$rateRequest->RequestedShipment->Recipient->Address->CountryCode = 'US';

		//shipping charges payment
		$rateRequest->RequestedShipment->ShippingChargesPayment->PaymentType = SimpleType\PaymentType::_SENDER;

		//rate request types
		$rateRequest->RequestedShipment->RateRequestTypes = [SimpleType\RateRequestType::_PREFERRED, SimpleType\RateRequestType::_LIST];

		$PackageCount = 1;	
		
		$rateRequest->RequestedShipment->PackageCount = $PackageCount;
		
		$rateRequest->RequestedShipment->RequestedPackageLineItems = [new ComplexType\RequestedPackageLineItem()];

		//package 1
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Value = 2;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Weight->Units = SimpleType\WeightUnits::_LB;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Length = 10;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Width = 10;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Height = 3;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->Dimensions->Units = SimpleType\LinearUnits::_IN;
		$rateRequest->RequestedShipment->RequestedPackageLineItems[0]->GroupPackageCount = 1;

		$rateServiceRequest = new FedexRequest();
		//$rateServiceRequest->getSoapClient()->__setLocation(FedexRequest::PRODUCTION_URL); //use production URL

		$rateReply = $rateServiceRequest->getGetRatesReply($rateRequest, true); // send true as the 2nd argument to return the SoapClient's stdClass response.


		if (!empty($rateReply->RateReplyDetails)) {
			foreach ($rateReply->RateReplyDetails as $rateReplyDetail) {
				//var_dump($rateReplyDetail->ServiceType);
				//echo $rateReplyDetail->ServiceType;
				if (!empty($rateReplyDetail->RatedShipmentDetails)) {
					foreach ($rateReplyDetail->RatedShipmentDetails as $ratedShipmentDetail) {
						echo ($rateReplyDetail->ServiceType.' => '.$ratedShipmentDetail->ShipmentRateDetail->RateType . ": " . $ratedShipmentDetail->ShipmentRateDetail->TotalNetCharge->Amount);
						echo "\n\n";
					}
				}
				//echo "<hr />";
			}
		}
		
		return response()->json([
			'message' => 'hello',
			'rate' => $rateReply
		], 200);
	}
	
	public function get_tracking_number(){
		
		$FEDEX_KEY = env('FEDEX_KEY');
		$FEDEX_PASSWORD = env('FEDEX_PASSWORD');
		$FEDEX_ACCOUNT_NUMBER = env('FEDEX_ACCOUNT_NUMBER');
		$FEDEX_METER_NUMBER = env('FEDEX_METER_NUMBER');
		$userCredential = new ComplexTypeShip\WebAuthenticationCredential();
		$userCredential
			->setKey($FEDEX_KEY)
			->setPassword($FEDEX_PASSWORD);
		$webAuthenticationDetail = new ComplexTypeShip\WebAuthenticationDetail();
		$webAuthenticationDetail->setUserCredential($userCredential);
		$clientDetail = new ComplexTypeShip\ClientDetail();
		$clientDetail
			->setAccountNumber($FEDEX_ACCOUNT_NUMBER)
			->setMeterNumber($FEDEX_METER_NUMBER);
		$version = new ComplexTypeShip\VersionId();
		$version
			->setMajor(23)
			->setIntermediate(0)
			->setMinor(0)
			->setServiceId('ship');
		$shipperAddress = new ComplexTypeShip\Address();
		$shipperAddress
			->setStreetLines(['Address Line 1'])
			->setCity('Austin')
			->setStateOrProvinceCode('TX')
			->setPostalCode('73301')
			->setCountryCode('US');
		$shipperContact = new ComplexTypeShip\Contact();
		$shipperContact
			->setCompanyName('Company Name')
			->setEMailAddress('test@example.com')
			->setPersonName('Person Name')
			->setPhoneNumber(('123-123-1234'));
		$shipper = new ComplexTypeShip\Party();
		$shipper
			->setAccountNumber($FEDEX_ACCOUNT_NUMBER)
			->setAddress($shipperAddress)
			->setContact($shipperContact);
		$recipientAddress = new ComplexTypeShip\Address();
		$recipientAddress
			->setStreetLines(['Address Line 1'])
			->setCity('Herndon')
			->setStateOrProvinceCode('VA')
			->setPostalCode('20171')
			->setCountryCode('US');
		$recipientContact = new ComplexTypeShip\Contact();
		$recipientContact
			->setPersonName('Contact Name')
			->setPhoneNumber('1234567890');
		$recipient = new ComplexTypeShip\Party();
		$recipient
			->setAddress($recipientAddress)
			->setContact($recipientContact);
		$labelSpecification = new ComplexTypeShip\LabelSpecification();
		$labelSpecification
			->setLabelStockType(new SimpleTypeShip\LabelStockType(SimpleTypeShip\LabelStockType::_PAPER_7X4POINT75))
			->setImageType(new SimpleTypeShip\ShippingDocumentImageType(SimpleTypeShip\ShippingDocumentImageType::_PDF))
			->setLabelFormatType(new SimpleTypeShip\LabelFormatType(SimpleTypeShip\LabelFormatType::_COMMON2D));
		$packageLineItem1 = new ComplexTypeShip\RequestedPackageLineItem();
		$packageLineItem1
			->setSequenceNumber(1)
			->setItemDescription('Product description')
			->setDimensions(new ComplexTypeShip\Dimensions(array(
				'Width' => 10,
				'Height' => 10,
				'Length' => 25,
				'Units' => SimpleTypeShip\LinearUnits::_IN
			)))
			->setWeight(new ComplexTypeShip\Weight(array(
				'Value' => 2,
				'Units' => SimpleTypeShip\WeightUnits::_LB
			)));
		$shippingChargesPayor = new ComplexTypeShip\Payor();
		$shippingChargesPayor->setResponsibleParty($shipper);
		$shippingChargesPayment = new ComplexTypeShip\Payment();
		$shippingChargesPayment
			->setPaymentType(SimpleTypeShip\PaymentType::_SENDER)
			->setPayor($shippingChargesPayor);
		$requestedShipment = new ComplexTypeShip\RequestedShipment();
		$requestedShipment->setShipTimestamp(date('c'));
		$requestedShipment->setDropoffType(new SimpleTypeShip\DropoffType(SimpleTypeShip\DropoffType::_REGULAR_PICKUP));
		$requestedShipment->setServiceType(new SimpleTypeShip\ServiceType(SimpleTypeShip\ServiceType::_FEDEX_GROUND));
		$requestedShipment->setPackagingType(new SimpleTypeShip\PackagingType(SimpleTypeShip\PackagingType::_YOUR_PACKAGING));
		$requestedShipment->setShipper($shipper);
		$requestedShipment->setRecipient($recipient);
		$requestedShipment->setLabelSpecification($labelSpecification);
		$requestedShipment->setRateRequestTypes(array(new SimpleTypeShip\RateRequestType(SimpleTypeShip\RateRequestType::_PREFERRED)));
		$requestedShipment->setPackageCount(1);
		$requestedShipment->setRequestedPackageLineItems([
			$packageLineItem1
		]);
		$requestedShipment->setShippingChargesPayment($shippingChargesPayment);
		$processShipmentRequest = new ComplexTypeShip\ProcessShipmentRequest();
		$processShipmentRequest->setWebAuthenticationDetail($webAuthenticationDetail);
		$processShipmentRequest->setClientDetail($clientDetail);
		$processShipmentRequest->setVersion($version);
		$processShipmentRequest->setRequestedShipment($requestedShipment);
		$shipService = new ShipService\Request();
		//$shipService->getSoapClient()->__setLocation('https://ws.fedex.com:443/web-services/ship');
		$result = $shipService->getProcessShipmentReply($processShipmentRequest);
		
		//var_dump($result);
		
		echo '<pre>';
		print_r($result);
		exit;
		/* return response()->json([
			'message' => 'Success!',
			'track_number' => $result
		], 200); */
		// Save .pdf label
		// file_put_contents('/path/to/label.pdf', $result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		//var_dump($result->CompletedShipmentDetail->CompletedPackageDetails[0]->Label->Parts[0]->Image);
		
	}
}
