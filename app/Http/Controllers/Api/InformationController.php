<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Information;
use Illuminate\Support\Facades\Mail;
use App\Mails\ContactEmail;
class InformationController extends Controller
{
    public function contact(Request $request,Contact $contact)
    {
        $validator = Validator::make($request->all(), [
            'f_name' => 'required|string|min:3|max:10',
            'l_name' => 'required|string|min:3|max:10',
            'email' => 'required|string|email',
            'contact_number' => 'required|string|min:5|max:14',
            'message' => 'nullable|string|min:10|max:250',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            $contact->f_name =$request->f_name;
            $contact->l_name =$request->l_name;
            $contact->email =$request->email;
            $contact->contact_number =$request->contact_number;
            $contact->message =strip_tags($request->message);
            if($contact->save()){
                $mail=Mail::to('gauravsh8790@gmail.com')->send(new ContactEmail($request->f_name,$request->l_name,$request->email,$request->contact_number,$request->message));
                return response()->json([
                    'message' => __("contact.success")
                ], 200);
            }else{
                return response()->json([
                    'message' => __("contact.failed"),
                ], 422);
            }
        }
    }
    public function about(Information $information)
    {
        if(Information::where('info_id','3')->exists()){
            return response()->json([
                'message' => __("contact.success"),
                'content'=>strip_tags(Information::find('3')->pluck('description'))
            ], 200);
        }
    }
    public function tNc(Information $information)
    {
        if(Information::where('info_id','1')->exists()){
            return response()->json([
                'message' => __("contact.success"),
                'content'=>strip_tags(Information::find('1')->pluck('description'))
            ], 200);
        }
    }
    public function Privacypolicy(Information $information)
    {
        if(Information::where('info_id','2')->exists()){
            return response()->json([
                'message' => __("contact.success"),
                'content'=>strip_tags(Information::find('2')->pluck('description'))
            ], 200);
        }
    }
}
