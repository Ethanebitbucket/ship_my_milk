<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Models\UsersConfig;
use App\Models\Kids;
use App\Traits\UtilityTrait;
use App\Libraries\WpHash;
use App\Mails\UserVerificationEmail;
use App\Http\Controllers\Controller;
use Image;
use Hash;

class UserController extends Controller
{
	use UtilityTrait;    
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user_details(Request $request)
    {
		$logged_user_id = $request->user()->id;
		$user = User::select('*')->where('users.id', $logged_user_id)->without('kids')->first();

        $user_details['user_detail']=$user;
        $user_details['user_detail']['kid_details']=$request->user()->kids;
        $user_details['user_detail']['user_config'] = $request->user()->UserConfig;
		return response()->json([
            'user_detail' => array_pop($user_details)
        ], 200);
    }
	
	public function edit_user(Request $request){
		$validator = Validator::make($request->all(), [
            'firstname' => 'required|string|min:2|max:60',
            'lastname' => 'required|string|min:2|max:60',
            'company_name' => 'nullable|string|min:2|max:255',
			'user_avatar' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', //optional
        ]);		
		
		if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);            
        }
		
		$user_id = $request->user()->id;
		
		//$user = User::find($user_id);		
		$user = User::select('users.*','users.user_avatar as profile_image')->where('users.id', $user_id)->first();
		
		if(empty($user)){
			return response()->json([
                'message' => 'User does not exist'
            ], 422);
		}
		
		$user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->company_name = $request->company_name;
		if(!empty($_FILES['user_avatar'])) {
            // REMOVE OLD IMAGE 
			@unlink(public_path('avatar/') . $user->profile_image);
			$user->user_avatar = null;
			$imageName = @md5(mt_rand().time()). '.' . $request->user_avatar->getClientOriginalExtension();
			$request->user_avatar->move(public_path('avatar'), $imageName);
			$user->user_avatar = $imageName;
			//$this->generate_device_images(public_path('avatar'), $imageName);               
        }
		$user->save();
		
		return response()->json([
            'message' => 'User Details Updated Successfully!',
            'user_data' => $user
        ], 200);
	}
	
	public function change_profile_image(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_avatar' => 'nullable|image|mimes:jpeg,png,jpg|max:2048', //optional
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first()
            ], 422);
        }
		
		$user_id = $request->user()->id;
        $user = User::select('users.*','users.user_avatar as profile_image')->where('users.id', $user_id)->first();
		
		if(empty($user)){
			return response()->json([
                'message' => 'User does not exist'
            ], 422);
		}
		
		// USER AVATAR CHANGE
		if(!empty($_FILES['user_avatar'])) {
			// REMOVE OLD IMAGE
			@unlink(public_path('avatar/') . $user->profile_image);
			$user->user_avatar = null;
			$imageName = @md5(mt_rand().time()). '.' . $request->user_avatar->getClientOriginalExtension();
			$request->user_avatar->move(public_path('avatar'), $imageName);
			$user->user_avatar = $imageName;
			//$this->generate_device_images(public_path('avatar'), $imageName);
			
			$user->save();
		}
		
		return response()->json([
			'message' => 'User Details Updated Successfully!',
			'user_data' => $user
		], 200);
    }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|same:confirm_password',
            'old_password' => 'required|string',
            'email' => 'required|string|email|unique:users,email,'.$request->user()->id,
            'country_code' => 'required',
            'contact_no' => 'required|unique:users,contact_no,'.$request->user()->id,
        ]);	
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            if($request->user()->id){
                if((Hash::check($request->old_password, $request->user()->password))){
                    $request->user()->password=bcrypt($request->password);
                    if($request->country_code!=$request->user()->country_code){
                        
                        $request->user()->country_code=$request->country_code;
                    }
                    if($request->contact_no!=$request->user()->contact_no){
                        $request->user()->contact_no=$request->contact_no;
                        
                    }
                    if($request->email!=$request->user()->email){
                        $wp_hasher = new WpHash(8, true);
                        $key = mt_rand(10000000, 99999999); //--> OUR LOGIC
                        $user_activation_key = time() . ':' . $wp_hasher->HashPassword($key);
                        //$request->user()->email=$request->email;
                        //$request->user()->active = false;
                        $request->user()->activation_key = $user_activation_key;
                        $url = url('/api/auth/signup/reactivate?token=' . $user_activation_key.'&email='.$request->email);
                        Mail::to($request->email)->send(new UserVerificationEmail($request->user()->firstname, $url));
                    }
                    if($request->user()->save()){
                        return response()->json([
                            'message' => __("home.password_save_msg"),
                        ], 200);
                    }else{
                        return response()->json([
                        'message' => __("home.password_not_save_msg")
                        ], 422);
                    }
                }else{
                    return response()->json([
                        'message' => __("home.old_password_not_matched")
                    ], 422);
                }
            }else {
                return response()->json([
                    'message' => __("home.user_not_exist_msg")
                ], 422);
            }

        }
    }
}