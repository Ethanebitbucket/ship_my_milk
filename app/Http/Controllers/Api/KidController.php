<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Kids;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class KidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Kids $kids,User $user)
    {
        if(Kids::where('user_id',$request->user()->id)->exists()){
            return response()->json([
                'message' => __("kids.user_kids_msg"),
                'kid_details' => $request->user()->kids
            ], 200);
        }else{
            return response()->json([
                'message' => __("kids.kids_user_not_exist_msg")
            ], 422);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Kids $kids,User $user)
    {
        $validator = Validator::make($request->all(), [
            'kid_name'=>'required|string|min:2|max:60',
            'kid_gender'=>'nullable|string|min:4|max:6',
            'kid_dob'=>'nullable|string|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }else{
            if(User::where('id',$request->user()->id)->exists()){
                $kids->kid_name=$request->kid_name;
                $kids->kid_gender=$request->kid_gender;
                $kids->kid_dob=$request->kid_dob;
                $kids->user_id=$request->user()->id;
                if($kids->save()){
                    return response()->json([
                        'message' => __("kids.kids_save_msg"),
                        'kid_details' => $request->user()->kids
                    ], 200);
                }else{
                    return response()->json([
                        'message' => __("kids.kids_not_save_msg")
                    ], 422);
                }
            }else{
                return response()->json([
                    'message' => __("kids.kids_user_not_exist_msg")
                ], 422);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kids  $kids
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Kids $kids,$kid_id)
    {
        /* $validator = Validator::make($request->all(), [
            'kid_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }*/
        if(Kids::where('id',$request->kid_id)->exists()){
            return response()->json([
                'message' => __('kids.kid_found_msg'),
                'kid_details' => Kids::find($request->kid_id)
            ], 200);
        }else{
            return response()->json([
                'message' => __("kids.kids_not_exist_msg")
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kids  $kids
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kids $kids,User $user)
    {
        $validator = Validator::make($request->all(), [
            'kid_id' => 'required|numeric',
            'kid_name'=>'required|string|min:2|max:60',
            'kid_gender'=>'nullable|string|min:4|max:6',
            'kid_dob'=>'nullable|string|date_format:Y-m-d H:i:s',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        }else{
            if(User::where('id',$request->user()->id)->exists()){
                if(Kids::where('id',$request->kid_id)->exists()){
                    $kids=Kids::find($request->kid_id);
                    $kids->kid_name=$request->kid_name;
                    $kids->kid_gender=$request->kid_gender;
                    $kids->kid_dob=$request->kid_dob;
                    $kids->user_id=$request->user()->id;
                    if($kids->save()){
                        return response()->json([
                            'message' => __("kids.kids_save_msg"),
                            'kid_details' => $request->user()->kids
                        ], 200);
                    }else{
                        return response()->json([
                            'message' => __("kids.kids_not_save_msg")
                        ], 422);
                    }
                }else{
                    return response()->json([
                        'message' => __("kids.kids_not_exist_msg")
                    ], 422);
                }
            }else{
                return response()->json([
                    'message' => __("kids.kids_user_not_exist_msg")
                ], 422);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kids  $kids
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Kids $kids,User $user,$kid_id)
    {
        /* $validator = Validator::make($request->all(), [
            'kid_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } */
        if(User::where('id',$request->user()->id)->exists()){
            if(Kids::where('id',$kid_id)->exists()){
                if(Kids::destroy($kid_id)){
                    return response()->json([
                        'message' => __("kids.kid_deleted_msg"),
                        'kid_details' => $request->user()->kids
                    ], 200);
                }else{
                    return response()->json([
                        'message' => __("kids.kids_not_deleted_msg"),
                    ], 422);
                }
            }else{
                return response()->json([
                    'message' => __("kids.kids_not_exist_msg")
                ], 422);
            }
        }else{
            return response()->json([
                'message' => __("kids.kids_user_not_exist_msg")
            ], 422);
        }

    }
}
