<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Box;
use App\Models\BoxPrice;
use App\Models\ServicePrice;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Video;
use App\Models\Region;
use Illuminate\Support\Facades\Validator;
use App\Traits\FedexTrait;

class HomeController extends Controller {
	
	use FedexTrait;

    public function index(Service $service, Box $box, Slider $slider, Region $region, Video $video) {
        return response()->json([
                'message' => __("home.home_details_msg"),
                'services' => Service::all(),
                'sliders' => Slider::ofType('1')->get(),
                'boxes' => Box::all(),
                'regions' => Region::all(),
                'video' => Video::first()
                ], 200);
    }

    public function get_cart_price(Request $request, Service $service, Box $box, BoxPrice $boxprice, ServicePrice $serviceprice) {
        $validator = Validator::make($request->all(), [
                'service_id' => 'required|digits_between:1,3',
                'region_id' => 'required|digits_between:1,3',
                'box_details' => 'required|array|min:1|max:2',
                'from_address' => 'nullable|array',
                'to_address' => 'required|array',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 422);
        } else {
            $boxes_price = array();
            if (Service::where('service_id', $request->service_id)->exists()) {
                $service_price = ServicePrice::where('service_id', $request->service_id)->first();
                if ($request->service_id == '1') {
                    $service_flag = true;
                } else {
                    $service_flag = false;
                    ;
                }
            } else {
                return response()->json([
                        'message' => __("home.service_not_exists")
                        ], 422);
            }
            if ($box_details = $request->box_details) {
				
				//flag 4, calculate shipping for boxes
				$is_shipping = false;
				
				//To shipping address
				$to_address = array(
					'address1' => $request->to_address['address1'],
					'city' => $request->to_address['city'],
					'province_code' => $request->to_address['state'],
					'zipcode' => $request->to_address['zipcode'],
					'country_code' => $request->to_address['country'],
				);
				
				//From shipping address
				//Note: if service id is 1, then `from address` will be client(application) address 
				if ($request->service_id == '1') {
					//Application default address 
					$from_address = array(
						'address1' => 'Ethane web technology',
						'city' => 'Noida',
						'province_code' => 'Uttar Pradesh',
						'zipcode' => '201301',
						'country_code' => 'India',
					);
				} else {
					$from_address = array(
						'address1' => $request->from_address['address1'],
						'city' => $request->from_address['city'],
						'province_code' => $request->from_address['state'],
						'zipcode' => $request->from_address['zipcode'],
						'country_code' => $request->from_address['country'],
					);
				}
                $bottle_shipping_charge =0;
                foreach ($box_details as $boxes) {
                    if (Box::where('box_id', $boxes['box_id'])->exists()) {
                        $boxprice = BoxPrice::where([['box_id', '=', $boxes['box_id']], ['region_id', '=', $request->region_id]])->first();
                        $box_type = $boxprice->box->box_type;
						
						//shipping charge calculation from fedex
						//$bottle_shipping_charge = $this->get_service_rate($is_shipping, $from_address, $to_address, array('weight'=>$boxprice->box->box_weight, 'length'=>$boxprice->box->box_length, 'width'=>$boxprice->box->box_width, 'height'=>$boxprice->box->box_height));
                        $boxes_price[$box_type]['price'] = round(($boxprice->box_price + $bottle_shipping_charge) * $boxes['qty'],2);
                        $boxes_price[$box_type]['qty'] = $boxes['qty'];
                    } else {
                        return response()->json([
                                'message' => __("home.box_not_exists")
                                ], 422);
                    }
                }
            }

            // Here Fedex Shipping Api will implement for caclculate shipping price
            $tax_percent = 9.25; //Will Dynamic from Admin Setting
            $total = array_sum(array_column($boxes_price, 'price')) + $service_price->service_price;
            $tax_amount = round(($tax_percent / 100) * $total,2);

            //Will Dynamic from Fedex
            $shipping_charge = 0;

            return response()->json([
                    'message' => __("home.cart_total_details_msg"),
                    'boxes_price' => $boxes_price,
                    'service_price' => $service_price->service_price,
                    'service_flag' => $service_flag,
                    'shipping_charge' => strval($shipping_charge),
                    'box_fees' => strval($total),
                    'tax_percent' => $tax_percent . '%',
                    'tax_amount' => strval($tax_amount),
                    'total' => strval(round(($total + $tax_amount + $shipping_charge),2))
                    ], 200);
        }
    }
}
