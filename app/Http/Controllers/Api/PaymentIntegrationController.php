<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Braintree;
class PaymentIntegrationController extends Controller
{
	public $gateway;
	public function __construct(){
		$config = new Braintree\Configuration([
			'environment' => env('BRAINTREE_ENV'),
			'merchantId' => env('BRAINTREE_MERCHANT_ID'),
			'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
			'privateKey' => env('BRAINTREE_PRIVATE_KEY')
		]);
		$this->gateway = new Braintree\Gateway($config);
		
	}
	
	public function brainTreeGetToken(){
		$clientTokenObj = $this->gateway->ClientToken();
		$clientToken = $clientTokenObj->Generate();
		return response()->json([
			'message' => 'Token generated successfully',
			'token' => $clientToken
		], 200);
	}
	
	public function brainTreePurchaseUrl(Request $request){	
		
		$result = $this->gateway->transaction()->sale([
			'amount' => $request->amount,
			'paymentMethodNonce' => $request->payment_method_nonce,
			'options' => [ 'submitForSettlement' => true ]
		]);
		
		return response()->json([
            'message' => 'Purchase done',
			'token' => $result
        ], 200);
	}
	
	public function brainTreeSubscriptionPlans(Request $request){
		$plans = $this->gateway->plan()->all();
		
		if(!isset($plans[0])){
			return response()->json([
				'message' => 'Subscription plan not found'
			], 422);
		}
		
		$my_plan = array(
			'plan_id' => $plans[0]->id,
			'plan_name' => $plans[0]->name,
			'plan_price' => $plans[0]->price,
		);

		return response()->json([
            'message' => 'Subscription Plans',
			'my_plan' => $my_plan
        ], 200);
	}
	
	public function brainTreeSubscriptionUrl(Request $request){	
		
		//Create Customer
		$customer = $this->gateway->customer()->create([
			'firstName' => $request->firstname,
			'lastName' => ($request->lastname)?$request->lastname:'',
			'company' => ($request->company)?$request->company:'',
			'email' => $request->email,
			'phone' => $request->contact_no
		]);
		
		//Generate paymentMethod Token
		$paymentMethod = $this->gateway->paymentMethod()->create([
			'customerId' => $customer->customer->id,
			'paymentMethodNonce' => $request->payment_method_nonce
		]);
		
		//Get Plans
		$plans = $this->gateway->plan()->all();
		$planId = $plans[0]->id;
		
		//Create Subscription And Payment Done
		$subscription = $this->gateway->subscription()->create([
			'paymentMethodToken' => $paymentMethod->paymentMethod->token,
			'planId' => $planId
		]);
		
		return response()->json([
            'message' => 'Successfully Subscribed!',
			'token' => $subscription
        ], 200);
	}
}