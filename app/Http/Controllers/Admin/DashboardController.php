<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use DB;

class DashboardController extends Controller
{
    public function home(Request $request, User $user){

        $all['users'] = User::select('*')
                                    ->leftJoin('users_config', 'users_config.config_user_id','=','users.id')->orderby('id','desc')->get()->toArray();                        

        return view('admin.dashboard', $all);

    }

    public function listing(Request $request){

        $all = DB::table('videos')->select('*')->orderby('video_id','desc')->get();

        return view('admin.videos',compact('all'));

    }


    public function changevideostatus(Request $request, $id, $status){

     $id = base64_decode($id);

     $chk = DB::table('videos')->select('*')->where('video_id',$id)->get();

     if(count($chk) != 0){

        $update_Status = DB::table('videos')->where('video_id',$id)->update(['status'=>$status]);

        if($update_Status){

            return redirect('home/video-list');

        }

     }else{
        return redirect('home/video-list');
     }

     
    }


    public function updatevideo(Request $request, $id){

        $id = base64_decode($id);

        $get_data = DB::table('videos')->where('video_id', $id)->get()->first();

        return view('admin.update_video', \compact('get_data'));


    }

    public function logout(Request $request){
        
        Session::forget(Config::get('constants.admin_session_key'));
        
        return redirect('admin/login')->with('err-msg', 'Good Bye!');
        
    }


    public function callupdate(Request $request){

        $id = $request->video_id;

        $link = $request->link;

        $status = $request->status;

        $video_id = explode("?v=", $link); 
        if (empty($video_id[1]))
            $video_id = explode("/v/", $link);

        $video_id = explode("&", $video_id[1]);
        $video_id = $video_id[0];

        $chk_first = DB::table('videos')->where('video_id',$id)->get();

        if(count($chk_first) != 0){
            
            $update = DB::table('videos')->where('video_id', $id)->update(['youtube_video_id'=>$video_id, 'status'=>$status]);
                           
                return redirect('home/video-list')->with('message', 'Successfully updated');
            
        }else{

            return redirect('home/video-list')->with('message', 'Technical Error Occured');

        }
        
    
        
    }

   

}
