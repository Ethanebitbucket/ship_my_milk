<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Box;
use App\Models\Admin\Admin;
use App\Models\Service;
use App\Models\Contact;
use App\Models\Information;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;
class BoxController extends Controller
{
    public function main(Request $request){

        $data = Box::select('*')
                    ->leftJoin('regional_box_price', 'boxes.box_id','=','regional_box_price.box_id')
                    ->leftJoin('regions','regional_box_price.region_id','=','regions.region_id')->get();
        
        return view('admin.box',\compact('data'));

    }

    public function services(Request $request){

        $data = Service::select('*')
                    ->leftJoin('regional_service_price', 'services.service_id','=','regional_service_price.service_id')
                    ->leftJoin('regions','regional_service_price.region_id','=','regions.region_id')->get();
        
        return view('admin.service',\compact('data'));

    }

    public function contacts(Request $request){

        $data = Contact::orderby('contact_id', 'desc')->get();
        
        return view('admin.contact',\compact('data'));

    }

    public function information(Request $request){

        $data = Information::all();
        
        return view('admin.information',\compact('data'));

    }

    public function informationupdate(Request $request, $id){

        $id =  \base64_decode($id);

        //$data = Information::where('info_id', $id)->get()->first();
        $data = Information::find($id);

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
               
                'description' =>  'required',

            ]);
            
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }    

            $data->description = $request->description;

            if($data->save()){

                return redirect('home/information')->with('msg', 'Successfully update');

            }

        }

        return view('admin.infoform', \compact('data'));    

    }

    public function boxupdate(Request $request, $id){

        
        $id = \base64_decode($id);

        $data = Box::find($id);
        
        if($request->isMethod('post')){

            $regex = "/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/";

            $validator = Validator::make($request->all(), [
               
                'height' =>  'required|integer',
                'width' =>  'required|integer',
                'length' =>  'required|integer',
                'weight' =>  'required|numeric|between:0,99.99',
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }    

            $data->box_height = $request->height;
            $data->box_width  = $request->width;
            $data->box_length = $request->length;
            $data->box_weight = $request->weight;

            if($data->save()){

                return redirect('home/box')->with('msg', 'Successfully update');

            }

        }

        return view('admin.extend_box', \compact('data'));

    }


    public function boxinfo(Request $request, $id){

        $id = base64_decode($id);

        $get = DB::table('regional_box_price')->where('box_plan_id', $id)->get()->first();

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
               
                'price'          =>  'required',
            ]);
    
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $update = DB::table('regional_box_price')->where('box_plan_id', $id)->update(['box_price'=>$request->price]);

            //if($update){

                return redirect('home/box')->with('msg', 'Successfully update');

            //}

        }


        return view('admin.editbox', compact('get'));

    }

    public function adminupdate(Request $request){

        $admin = Admin::find(7);
        
        if($request->isMethod('post')){
            
            $validator = Validator::make($request->all(), [
               
                'fname'          =>  'required',
                'lname'          =>  'required',
                'email'          =>  'required|email',
                'password'       =>  'required',
                'contact'        =>  'required',
                'tax_percentage' =>  'required',
                'address'        =>  'required',
                'c_code'         =>  'required',
                's_code'         =>  'required',
                'z_code'         =>  'required',
                'city'           =>  'required',
            ]);

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $admin->firstname = $request->fname;
            $admin->lastname = $request->lname;
            $admin->email = $request->email;
            $admin->contact = $request->contact;
            $admin->password = Hash::make($request->password);
            $admin->tax_percentage = $request->tax_percentage;
            $admin->address = $request->address;
            $admin->country_code = $request->c_code;
            $admin->state_code = $request->s_code;
            $admin->zip_code = $request->z_code;
            $admin->city = $request->city;
            
            
            return redirect('home/admin')->with('msg', 'Successfully update');

            
        }

        return view('admin.admin', \compact('admin'));
        
    }


    public function userorder(Request $request){

        $data = Order::select('*')
                            ->leftJoin('order_details', 'order_details.order_id','=','orders.order_id')
                            ->leftJoin('services', 'services.service_id', '=', 'orders.service_id')
                            ->leftJoin('regions', 'regions.region_id', '=', 'orders.region_id')->get();
           
        return view('admin.order', compact('data'));
    }

    public function updateservice(Request $request, $id){

        $name = DB::table('services')->where('service_id', $id)->get()->first();
        $price = DB::table('regional_service_price')->where('service_id', $id)->get()->first();
        
        
        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
               
                'service'          =>  'required',
                'price'          => 'required',
            ]);
            
            

            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $name = DB::table('services')->where('service_id', $id)->update(['service_name'=>$request->service]);
            $price = DB::table('regional_service_price')->where('service_id', $id)->update(['service_price'=>$request->price]);

            return redirect('home/services')->with('msg', 'Successfully update');

        }

        return view('admin.serviceupdate', compact('name','price'));

    }

}
