<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class AdminController extends Controller
{
    public function login(Request $request){

        if($request->isMethod('post')){

            $validator = Validator::make($request->all(), [
               
                'email' =>  'required|string|email|max:255',
                'password'  =>  'required|string'
                
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            

            $admin = Admin::where('email',$request->email)->first();
            
            if($admin){
                
                $name = ucfirst($admin->firstname).' '.ucfirst($admin->lastname);
                
                $matched = Hash::check($request->password, $admin['password']);
                
                if($matched){
                    
                    Session::put(Config::get('constants.admin_session_key'), $admin);
                    
                    return redirect('home/dashboard');
                    //return redirect('admin/home/dashboard')->with('success-msg', 'Welcome '.' '.$name);
                    
                }else{
                    
                    return redirect('admin/login')->with('err-msg', 'Incorrect Email / Password Provided');

                }
                
            }else{
                
                return redirect('admin/login')->with('err-msg', 'Account with this email doest not exist');
                
            }

        }

        return view('admin.login');

    }
}
