<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $f_name, $l_name,$email,$contact_number,$message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($f_name,$l_name,$email,$contact_number,$message)
    {
        $this->f_name =$f_name;
        $this->l_name =$l_name;
        $this->email =$email;
        $this->contact_number =$contact_number;
        $this->message =strip_tags($message);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contact Email | Ship My Milk')->view('email.contact');
    }
}
