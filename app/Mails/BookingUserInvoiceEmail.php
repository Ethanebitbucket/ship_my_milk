<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingUserInvoiceEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $firstname, $invoice_path, $booking_no;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstname, $invoice_path, $booking_no)
    {
        $this->firstname = $firstname;
        $this->invoice_path = $invoice_path;
        $this->booking_no = $booking_no;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('CTBF #' . $this->booking_no . ' Booking Confirmed')
        ->view('auth.mail.bookingconfirmation')
        ->attach($this->invoice_path);
    }
}
