<?php

namespace App\Mails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserVerificationEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $firstname, $verification_link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstname, $link)
    {
        $this->firstname = $firstname;
        $this->verification_link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Account Confirmation | Ship My Milk')->view('auth.mail.userverification');
    }
}
