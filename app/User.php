<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
	 
	protected $primaryKey = 'id';
	protected $table = 'users';
	public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 
		'lastname', 
		'company_name', 
		'email', 
		'country_code', 
		'contact_no', 
		'user_avatar',
		'password',
		'active',
        'activation_key',
        'remember_token',
        'device_token',
        'device_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'activation_key',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	public function getUserAvatarAttribute($value)
    {
        if($value == '' || is_null($value)) {
            return null;
        } else {
            return url('/avatar/' . $value);
        }
    }

    public function kids()
    {
        return $this->hasMany('App\Models\Kids');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\Order')->orderBy('created_at', 'desc');
    }
    public function UserConfig()
    {
        return $this->hasOne('App\Models\UsersConfig','config_user_id');
    }
}
