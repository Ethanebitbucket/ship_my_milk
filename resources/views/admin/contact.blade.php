@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Contact List </h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Message</th>
                <th>Date</th>
            </thead>
         
            <tbody>
              
                @if(isset($data) && !empty($data))
                @foreach($data as $d)

                <tr>
                    <td>{{ $d->f_name.' '.$d->l_name }}</td>
                    <td>{{ $d->email }}</td>
                    <td>{{ $d->contact_number }}</td>
                    <td>{{ $d->message }}</td>
                    <td><?php echo date('M d,Y',strtotime($d->created_at)); ?></td>
                </tr>

                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  