@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Box Dimensions</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
        <form action="{{ url('home/box-update/'.base64_encode($data->box_id)) }}" method="post"> 
        @csrf
            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="Height" name="height" value="{{ old('height') }}">
               </div>
               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="Width" name="width" value="{{ old('width') }}">
               </div>
                
            </div> 
            
            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="Length" name="length" value="{{ old('length') }}">
               </div>

               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="Weight" name="weight" value="{{ old('weight') }}">
               </div>
                
            </div>     

            <p style="color:red;font-size:15px;">{{ $errors->first('height') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('width') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('length') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('weight') }}</p>
            
            <div class="row form-group">
                <div class="col-sm-3 col-sm-offset-6"></div>
                <div class="col-sm-12 text-right">
                    <input type="submit" name="" value="Update" class="btn btn-primary"> 
                </div>
                
            </div>  
        </form>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  