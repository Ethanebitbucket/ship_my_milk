
@section('sidebar')
<!-- Sidebar -->
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

<a class="navbar-brand mr-1" href="{{ url('home/dashboard') }}">{{ ucfirst(session()->get('eth_admin_ship_my_milk')->firstname).' '.session()->get('eth_admin_ship_my_milk')->lastname }}</a>

<button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
  <i class="fas fa-bars"></i>
</button>

<!-- Navbar -->
<ul class="navbar-nav ml-auto d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0" >

  <li class="nav-item dropdown no-arrow text-right">
    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-user-circle fa-fw"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
      <!-- <a class="dropdown-item" href="#">Settings</a>
      <a class="dropdown-item" href="#">Activity Log</a> -->
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
      <a class="dropdown-item" href="{{ url('home/admin') }}">Admin Settings</a>
    </div>
  </li>
</ul>

</nav>

<div id="wrapper">

<ul class="sidebar navbar-nav">
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/dashboard') }}">
      <span>Dashboard</span>
    </a>
  </li>
  <li class="nav-item dropdown">
    <!-- <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-fw fa-folder"></i>
      <span>Pages</span>
    </a> -->
    <!-- <div class="dropdown-menu" aria-labelledby="pagesDropdown">
      <h6 class="dropdown-header">Login Screens:</h6>
      <a class="dropdown-item" href="login.html">Login</a>
      <a class="dropdown-item" href="register.html">Register</a>
      <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
      <div class="dropdown-divider"></div>
      <h6 class="dropdown-header">Other Pages:</h6>
      <a class="dropdown-item" href="404.html">404 Page</a>
      <a class="dropdown-item" href="blank.html">Blank Page</a>
    </div> -->
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/video-list') }}">
      <span>Video</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/box') }}">
      <span>Box</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/services') }}">
      <span>Service</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/information') }}">
      <span>Information</span></a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/order') }}">
      <span>Order</span></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ url('home/contacts') }}">
      <span>Contacts</span></a>
  </li>
  
  <!-- <li class="nav-item">
    <a class="nav-link" href="{{ url('home/country-list') }}">
    <i class='fa fa-globe'></i>
      <span>Country</span></a>
  </li> -->
 
</ul>

@stop