@extends('admin.dashboard_base')
@section('title', 'Country')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
      

    @if(Session::has('message'))
    <div class=" btn-success" width="100%">
    <h5>{{ Session::get('message') }}</h5>
      </div>
    @endif
    <!-- Breadcrumbs-->
    
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Country List</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
      <div class="table-responsive">
      <table class="table table-boarder">
    <thead class="thead-light">
      <tr>
        <th><input type="text" class="form-control" placeholder="Country"></th>
        <th><input type="text" class="form-control" placeholder="State"></th>
        <th><input type="text" class="form-control" placeholder="City"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
</div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  