@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Box Edit</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
        <form action="" method="post"> 
        @csrf
            <div class="row form-group col-m-12">
                
               <div class="col-sm-12">
                   <input type="text" class="form-control" placeholder="price" name="price" value="{{ $get->box_price, old('price') }}">
               </div>
                
            </div> 
            
            <p style="color:red;font-size:15px;">{{ $errors->first('price') }}</p>
            
            <div class="row form-group">
                <div class="col-sm-3 col-sm-offset-6"></div>
                <div class="col-sm-12 text-right">
                    <input type="submit" name="" value="Update" class="btn btn-primary"> 
                </div>
                
            </div>  
        </form>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  