@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
  @if(Session::has('message'))
    <div class=" btn-success" width="100%">
    <h5>{{ Session::get('message') }}</h5>
      </div>
    @endif
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Video List</h5>
      </li>
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">Sr</th>
                <th class="text-center">Video Link</th>
                <th class="text-center">Status</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
         
            <tbody>
               
                @if(isset($all) && !empty($all))
                @foreach($all as $list)
                @php ($num = 1)
                <tr>
                    <td class="text-center">{{ $num++ }}</td>
                    <td class="text-center"><a href="{{ 'https://www.youtube.com/watch?v='.$list->youtube_video_id }}" target="_blank">Click to view</a></td>
                    <td class="text-center">
                        @if($list->status == 1)
                        <a href="{{ url('home/change-status'.'/'.base64_encode($list->video_id).'/0') }}" class="btn btn-danger btn-sm">Deactive</a>
                        @else
                        <a href="{{ url('home/change-status'.'/'.base64_encode($list->video_id).'/1') }}" class="btn btn-success btn-sm">Active</a>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ url('home/update-video'.'/'.base64_encode($list->video_id)) }}" title="edit"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>

                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  