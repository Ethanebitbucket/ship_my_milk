@extends('admin.base')
@section('title', trans('admin/login.page_title'))

@section('content')
@yield('sidebar')
<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">{{ trans('admin/login.page_title') }}</div>
      <div class="card-body">
      @if(Session::has('msg-error'))
      {{ Session::get('msg-error') }}
      @endif
        <form action="{{ url('admin/login') }}" method="post">
        @csrf
          <div class="form-group">
            <div class="form-label-group">
              <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" autofocus="autofocus" autocomplete="off">
              <label for="inputEmail">Email address</label>
            </div>
            <p style="color:red">{{ $errors->first('email') }}</p>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" >
              <label for="inputPassword">Password</label>
            </div>
            <p style="color:red">{{ $errors->first('password') }}</p>
          </div>
          <button class="btn btn-primary btn-block" type="submit">{{ trans('admin/login.page_title') }}</button>
          
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="#">{{ trans('admin/login.register') }}</a>
          <a class="d-block small" href="#">{{ trans('admin/login.forget') }}</a>
        </div>
      </div>
    </div>
  </div>
@stop