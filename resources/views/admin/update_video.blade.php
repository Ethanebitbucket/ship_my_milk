@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
      

    @if(Session::has('message'))
    <div class=" btn-success" width="100%">
    <h5>{{ Session::get('message') }}</h5>
      </div>
    @endif
    <!-- Breadcrumbs-->
    
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Users</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
        
        <form action="{{ url('home/updatevideo') }}" method="post">
        @csrf
        <input type="hidden" name="video_id" value="{{ $get_data->video_id }}">
        <div class="row form-group">
            <div class="col-sm-12 col-sm-offset-3">
                <input type="text" class="form-control" value="{{ 'https://www.youtube.com/watch?v='.$get_data->youtube_video_id }}" name="link">
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12 col-sm-offset-3">
                <select class="form-control" name="status">
                    @if($get_data->status == '1')
                    <option value="1" selected disabled>Active</option>
                    @else
                    <option value="0" selected disabled>Dective</option>
                    @endif
                    <option value="1">Active</option>
                    <option value="0">Deactive</option>
                </select>
            
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-12 col-sm-offset-3">
                
                <button type="submit" class="btn btn-primary btn-block">Update</button>
                    
            </div>
        </div>

        </form>

      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  