@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Dashboard (users list)</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">User Name</th>
                <th class="text-center">User Company</th>
                <th class="text-center">User Email</th>
                <th class="text-center">Contact</th>
                <th class="text-center">Notification Status</th>
                <th class="text-center">User Primary Contact</th>
              </tr>
            </thead>
         
            <tbody>
              
                @if(isset($users) && !empty($users))
                @foreach($users as $user)

                <tr>
                    <td class="text-center">{{ $user['firstname'].' '.$user['lastname'] }}</td>
                    <td class="text-center">{{ $user['company_name'] }}</td>
                    <td class="text-center">{{ $user['email'] }}</td>
                    <td class="text-center">{{ $user['contact_no'] }}</td>
                    <td class="text-center">
                      @if($user['config_mute_notification'] == 0)
                      <p>Off</p>
                      @elseif($user['config_mute_notification'] == 1)
                      <p>On</p>
                      @else
                      <p>N.A</p>
                      @endif
                    </td>

                    <td class="text-center">
                      @if($user['config_primary_contact'] == 0)
                      <p>Email</p>
                      @elseif($user['config_primary_contact'] == 1)
                      <p>Phone</p>
                      @else
                      <p>N.A</p>
                      @endif
                    </td>
                </tr>

                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  