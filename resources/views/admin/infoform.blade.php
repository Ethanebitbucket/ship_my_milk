@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Edit Information Page</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
        <form action="{{ url('home/information-update/'.base64_encode($data->info_id)) }}" method="post"> 
        @csrf
            <div class="row form-group">
                
                <input type="hidden" name="info_id" value="{{ $data->info_id }}">
                <label for="" class="col-sm-2 label-control">Description</label>
                <textarea class="col-sm-9 form-control" rows="10" name="description">{{ $data->description }}</textarea>
                
            </div>  
            <p style="color:red;font-size:15px;">{{ $errors->first('description') }}</p>
            <div class="row form-group">
                <div class="col-sm-3 col-sm-offset-6"></div>
                <div class="col-sm-12 text-right">
                    <input type="submit" name="" value="Update" class="btn btn-primary"> 
                </div>
                
            </div>  
        </form>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  