@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Admin</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      @if(Session::has('msg'))

        <div class="alert alert-warning">
          <strong>{{ Session::get('msg') }}</strong>
        </div>

      @endif

      <div class="card-body">
        <form action="{{ url('home/admin') }}" method="post"> 
        @csrf
            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="First Name" name="fname" value="{{ $admin->firstname, old('fname') }}">
               </div>
               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="Last Name" name="lname" value="{{ $admin->lastname, old('lname') }}">
               </div>
                
            </div> 
            
            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="Email" name="email" value="{{ $admin->email, old('email') }}">
               </div>

               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="Contact" name="contact" value="{{ $admin->contact, old('contact') }}">
               </div>
                
            </div>
            
            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="password" class="form-control" placeholder="Password" name="password" value="{{ $admin->password, old('password') }}">
               </div>
               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="Tax Percentage %" name="tax_percentage" value="{{ $admin->tax_percentage, old('tax_percentage') }}">
               </div>
                
            </div> 


            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="Country Code" name="c_code" value="{{ $admin->country_code, old('c_code') }}">
               </div>
               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="State Code" name="s_code" value="{{ $admin->state_code, old('s_code') }}">
               </div>
                
            </div> 


            <div class="row form-group col-m-12">
                
               <div class="col-sm-6">
                   <input type="text" class="form-control" placeholder="Zip Code" name="z_code" value="{{ $admin->zip_code, old('z_code') }}">
               </div>
               <div class="col-sm-6">
               <input type="text" class="form-control" placeholder="City" name="city" value="{{ $admin->city, old('city') }}">
               </div>
                
            </div> 
            
            <div class="row form-group col-m-12">
                
               <div class="col-sm-12">
                   
                   <textarea class="form-control" rows="5" placeholder="Adress" name="address">{{ $admin->address, old('address') }}</textarea>
               </div>
                
              
                
            </div>

            <p style="color:red;font-size:15px;">{{ $errors->first('fname') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('lname') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('email') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('contact') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('password') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('tax_percentage') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('c_code') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('s_code') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('z_code') }}</p>
            <p style="color:red;font-size:15px;">{{ $errors->first('city') }}</p>
  
            <div class="row form-group">
                <div class="col-sm-3 col-sm-offset-6"></div>
                <div class="col-sm-12 text-right">
                    <input type="submit" name="" value="Update" class="btn btn-primary"> 
                </div>
                
            </div>  
            
        </form>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  