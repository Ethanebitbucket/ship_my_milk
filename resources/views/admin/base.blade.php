<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title') | Ship My Milk</title>

  <!-- Custom fonts for this template-->
  <link href="{{ url('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="{{ url('assets/css/sb-admin.css') }}" rel="stylesheet">

</head>

<body class="bg-dark">

@if(Session::has('err-msg'))

<div class="text-center" style="margin-top:5%">
<span class="login100-form-title p-b-20 msg-err" style="color:white;" >{{Session::get('err-msg')}}</span>
</div>

@endif

 @yield('content')

  <!-- Bootstrap core JavaScript-->
  <script src="{{ url('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ url('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

</body>

</html>
