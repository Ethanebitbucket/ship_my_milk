@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Information Pages</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
        
        @if(Session::has('msg'))

        <div class="alert alert-warning">
          <strong>{{ Session::get('msg') }}</strong>
        </div>

        @endif

      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">Page Title</th>
                <th class="text-center">Description</th>
                <th class="text-center">Edit</th>
            </thead>
         
            <tbody>
              
                @if(isset($data) && !empty($data))
                @foreach($data as $d)

                <tr>
                    <td class="text-center">{{ $d->page_title }}</td>
                    <td class="text-center">{{ $d->description }}</td>
                    <td class="text-center"><a href="{{ url('home/information-update/'.base64_encode($d->info_id)) }}">Edit</a></td>
                </tr>
                
                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  