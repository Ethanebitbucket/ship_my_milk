@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Order List</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
      
      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Service Name</th>
                <th>Region</th>
                <th>Order Amount</th>
                <th>Payment Status</th>
                <th>Order Status</th>
                <th>Payment Mode</th>
                <th>Alternate Number</th>
                <th>Tracking Number</th>
                <th>Origin Address </th>
                <th>Destination Address</th>
                <th>Currency</th>
                <th>Box Details</th>
                <th>Box Price</th>
                <th>Check In</th>
                <th>Check Out</th>
              </tr>
            </thead>
         
            <tbody>
              
                @if(isset($data) && !empty($data))
                @foreach($data as $d)

                <tr>
                    <td>{{ $d->service_name }}</td>
                    <td>{{ $d->region_name }}</td>
                    <td>{{ $d->amount }}</td>
                    <td>{{ $d->status }}</td>
                    <td>{{ $d->order_status }}</td>
                    <td>{{ $d->payment_mode }}</td>
                    <td>{{ $d->alternate_number }}</td>
                    <td>{{$d->tracking_no}}</td>
                    <td><?php echo $d->from_address ? implode (',',unserialize($d->from_address)): NULL;?>
                    </td>
                    <td><?php echo $d->to_address ? implode (',',unserialize($d->to_address)):null;?></td>
                    <td>{{ $d->currency }}</td>
                    <td><?php 
                    $udata = unserialize($d->box_details); 
                    echo $udata[0]['box_id'] == 1 ?'Box Type : Large' :'Box Type: Small';
                    echo '<br />';
                    echo 'Qty: '. $udata[0]['qty'];
                    ?></td>

                    <td><?php 
                    $udata = unserialize($d->box_price); 
                      echo '$'.$udata[0]['price'];
                    ?></td>

                    <td><?php echo date('M d,Y',strtotime($d->check_in_date)); ?></td>
                    <td><?php echo date('M d,Y',strtotime($d->check_out_date)); ?></td>
                </tr>

                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  