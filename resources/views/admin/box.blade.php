@extends('admin.dashboard_base')
@section('title', 'Dashboard')
@extends('admin.sidebar')
@section('content')

<div id="content-wrapper">

  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <h5>Boxes List</h5>
      </li>
     
    </ol>

    <!-- DataTables Example -->
    <div class="card mb-3">
    @if(Session::has('msg'))

      <div class="alert alert-warning">
        <strong>{{ Session::get('msg') }}</strong>
      </div>

    @endif
      <div class="card-body">
      <div class="table-responsive">
          <table class="table table-striped table-bordered dt-responsive nowrap" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th class="text-center">Box Type</th>
                <th class="text-center">Box Price</th>
                <th class="text-center">Box Subtitle</th>
                <th class="text-center">Box Size</th>
                <th class="text-center">Region Name</th>
                <th class="text-center">Action</th>
            </thead>
         
            <tbody>
              
                @if(isset($data) && !empty($data))
                @foreach($data as $d)

                <tr>
                    <td class="text-center">{{ $d->box_type }}</td>
                    <td class="text-center">{{ $d->box_price }}</td>
                    <td class="text-center">{{ $d->box_size }}</td>
                    <td class="text-center">{{ $d->box_subtitle }}</td>
                    <td class="text-center">{{ $d->region_name }}</td>
                    <td class="text-center">
                      <a href="{{ url('home/box-update/'.base64_encode($d->box_id)) }}"><i class="fa fa-plus"></i></a>&nbsp;&nbsp;
                      <a href="{{ url('home/box-info/'.base64_encode($d->box_plan_id)) }}"><i class="fa fa-edit"></i></a>
                    </td>
                </tr>

                @endforeach    
                @endif
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
  @stop

  