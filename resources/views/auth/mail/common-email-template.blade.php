<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Email Template</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <style>
		body{
			font-family: 'Roboto', sans-serif;
		}
        a {
            outline-style: none;
        }
        
        input {
            outline-style: none;
        }
        
        tr,
        td {
            padding: 0;
        }
        
        a {
            text-decoration: none;
            color: #a6a5a6;
        }
        
        p {
			margin:0;
        }
		h1, h2, h3, h4, h5, h6{
			margin: 0;
		}
        
        @media(max-width: 570px) {
            .main_table {
                width: 100% !important;
                max-width: 100% !important;
            }
            .full {
                width: 100%!important;
            }
            .tab {
                width: 100% !important;
            }
            .th1 {
                width: 20% !important;
            }
            .th2 {
                width: 80% !important;
                font-size: 11px !important;
            }
            .th2 a,
            .th2 p {
                font-size: 12px !important;
            }
        }
        
        @media(max-width: 350px) {
            .th1 {
                width: 15% !important;
            }
            .th2 {
                width: 85% !important;
                padding-left: 10px!important;
            }
        }
    </style>
</head>
<body bgcolor="#f5f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table class="main_table" border="0" cellpadding="0" cellspacing="0" width="600" align="center"style="background-color: #e0e0e0;border: 0px;">
		<thead>
            <tr style="background: #000;">
                <td style=" padding:15px; text-align: center;"><img src="{{ url('/public/images/logo.png') }}" style="width:106px;" /></td>
			</tr>
		</thead>
		<tbody  style="background: #FFF;">
            <tr>
				<td style="font-size:20px;padding:20px 20px 10px 20px;text-align: left;color: #000;">Dear,  <?php if(isset($name) && !empty($name)){ echo $name; }?></td>
			</tr>
			<tr>
                <td style="padding: 0 20px 20px 20px; text-align: center;color: #4f5e6e; font-size: 12px;line-height: 1.7;letter-spacing: 1px;">
					<p style="margin-bottom:15px;"><?php if(isset($content) && !empty($content)){ echo $content; } ?></p> 
					<?php if(isset($candidate_name) && !empty($candidate_name)){ ?>
						
						<table style="width:90%;margin:auto; color:#4f5e6e; font-size:12px;">
							<tr>
								<td style="width:170px; padding-bottom:10px; font-weight:bold;">Candidate Name:</td>
								<td style="padding-bottom:10px; word-break:break-all;"><?php if(isset($candidate_name) && !empty($candidate_name)){ echo $candidate_name; } ?></td>
							</tr>
							<tr>
								<td style="width:170px; padding-bottom:10px; font-weight:bold;">Candidate Email:</td>
								<td style="padding-bottom:10px;word-break:break-all;"><?php if(isset($candidate_email) && !empty($candidate_email)){ echo $candidate_email; } ?></td>
							</tr>
						</table>
					<?php } ?>
					</br>
					<?php if(isset($candidate) && !empty($candidate)){ ?>
					<p style="text-align:left;"><b>Thank you,</b></p> 
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td style="padding: 20px 20px 5px;color: #4f5e6e; text-align: center;">To Contact us, please visit</td>
			</tr>
			<tr style="margin: 0px 20px 20px;">
				<td style="    padding: 5px 20px 15px;color: #4f5e6e;  text-align: center;"><a href="<?php echo URL('/'); ?>" style="color:#188abe;text-decoration: none;"><?php echo URL('/'); ?></a></td>
			</tr>
            <tr style="background: #000">
                <td>
                    <table width="100%" style="color: #fff;">
                        
                      <tr>
                          <td style="    padding:20px 0px 0px; text-align:center; font-size:13px;color:#FFF;line-height: 20px; text-align: center;">Follows Us: <a href="" style="color:#4661b6;text-decoration: none; font-size: 18px; margin-left: 5px;"><i class="fa fa-facebook"></i> </a>  <a href="" style="color:#1bb7eb;text-decoration: none;font-size: 18px; margin-left: 5px;"> <i class="fa fa-twitter"></i> </a>  <a href="" style="color:#96342c;text-decoration: none;font-size: 18px; margin-left: 5px;"> <i class="fa fa-google-plus"></i> </a>  </td>
                
			</tr>
            <tr>
                <td style="padding: 10px 20px 20px; text-align:center; font-size:13px;color:#4f5e6e;line-height: 20px; text-align: center;"><a href="" style="color:#FFF; margin-right: 5px;text-decoration: none;"> About us </a>  <a href="" style="color:#FFF; margin: 0px 5px;text-decoration: none;"> Privacy Policy </a>  <a href="" style="color:#FFF; margin-left: 5px;text-decoration: none;"> Terms & Conditions </a></td>
                
			</tr>  
                        
                        
                    </table>
                     </td>
            </tr>
			
			
		</tbody>
	</table>
</body>
</html>
