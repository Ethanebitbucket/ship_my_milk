<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Invoice - #{{ $subscription->invoice_no }}</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }
        body {
            margin: 0px;
        }
        * {
            font-family: Verdana, Arial, sans-serif;
        }
        a {
            color: #fff;
            text-decoration: none;
        }
        table {
            font-size: x-small;
        }
        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }
        .invoice table {
            margin: 15px;
        }
        .invoice h3 {
            margin-left: 15px;
        }
        .information {
            background-color: #60A7A6;
            color: #FFF;
        }
        .information .logo {
            margin: 5px;
        }
        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <h3>{{ $subscribe_user->firstname }}</h3>
<pre>
{{ $location['location_name'] }}
{{ $location['city'] }}
{{ $location['country'] }}
<br /><br />
Date: {{ $subscription->created_at }}
Identifier: #{{ $subscription->invoice_no }}
Status: Paid
</pre>
            </td>
            <td align="center">
                <img src="{{public_path() . '/img/ctbf-logo.png'}}" alt="Logo" width="64" class="logo"/>
            </td>
            <td align="right" style="width: 40%;">

                <h3>CHOOSE TO BE FIT</h3>
                <pre>
                    www.choosetobefit.net
                    Worldwide Inc
                    Email: support@choosetobefit.net
                    Contact: 407-605-0816
                </pre>
            </td>
        </tr>

    </table>
</div>
<br/>
<div class="invoice">
    <h3>Booking No #{{ $subscription->invoice_no }}</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>Description</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $plan->plan }}</td>
            <td>1</td>
            <td align="left">{{ $subscription->amount_paid }}</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="left">Total</td>
            <td align="left" class="gray">{{ $subscription->amount_paid }}</td>
        </tr>
        </tfoot>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} - www.choosetobefit.net. All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                Your health & fitness pro is just a click away
            </td>
        </tr>
    </table>
</div>
</body>
</html>