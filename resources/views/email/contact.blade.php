<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>SMM ACCOUNT VERIFICATION</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
        <style>
		body{
			font-family: 'Roboto', sans-serif;
		}
        a {
            outline-style: none;
        }
        
        input {
            outline-style: none;
        }
        
        tr,
        td {
            padding: 0;
        }
        
        a {
            text-decoration: none;
            color: #a6a5a6;
        }
        
        p {
			margin:0;
        }
		h1, h2, h3, h4, h5, h6{
			margin: 0;
		}
        
        @media(max-width: 570px) {
            .main_table {
                width: 100% !important;
                max-width: 100% !important;
            }
            .full {
                width: 100%!important;
            }
            .tab {
                width: 100% !important;
            }
            .th1 {
                width: 20% !important;
            }
            .th2 {
                width: 80% !important;
                font-size: 11px !important;
            }
            .th2 a,
            .th2 p {
                font-size: 12px !important;
            }
        }
        
        @media(max-width: 350px) {
            .th1 {
                width: 15% !important;
            }
            .th2 {
                width: 85% !important;
                padding-left: 10px!important;
            }
        }
    </style>
</head>

<body bgcolor="#f5f5f5" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table class="main_table" border="0" cellpadding="0" cellspacing="0" width="600" align="center"style="background-color: #e0e0e0;border: 0px;">
		<thead>
            <tr style="background: #1B4588">
                <td style=" padding:15px; text-align: center;">
                    <img src="{{ url('/img/smm-logo.png') }}" style="width:106px;" />
                </td>
			</tr>
		</thead>
		<tbody style="background: #FFF;">
            <tr>
				<td style="font-size:20px;padding:20px 20px 10px 20px;text-align: left;color: #000;">Dear ,</td>
			</tr>
			<tr>
                <td style="padding: 0 20px 20px 20px; text-align: center;color: #4f5e6e; font-size: 12px;line-height: 1.7;letter-spacing: 1px;">
					<p style="margin-bottom:15px;">
                        We want to thank you for being a part of our <b>Ship My Milk</b> community! You are just one step closer to us. Please confirm your account before proceding further.
                    </p> 
						<table style="width:90%;margin:auto; color:#4f5e6e; font-size:12px;">
							<tr>
								<td style="width:170px; padding-bottom:10px; font-weight:bold; text-align:center;">
                                    <a href="" style="background:#0768d1; padding:5px 10px; border-radius:10px; color:#fff; text-decoration: none;">
                                        Confirm Me
                                    </a>
                                </td>
							</tr>
						</table>
					</br>
					
                        <p style="text-align:left;">
                            <b>Thank you once again for being a part of us. Your active participation will boost our moral to go high with our service.</b>
                        </p> 
				</td>
			</tr>
			<tr>
    			<td>
    				<table align="center" cellpadding="0" cellspacing="0" id="footer" style="background-color: #1B4588; width: 100%; padding: 0px 20px 30px;">
    					<tbody>
    						<tr>
    							<td style="-ms-text-size-adjust: 100%;
    								-webkit-font-smoothing: antialiased;
    								-webkit-text-size-adjust: 100%;
    								color: #FFF;
    								font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
    								font-size: 30px;
    								font-smoothing: always;
    								font-style: normal;
    								mso-line-height-rule: exactly;
    								text-decoration: none;
    								padding-top: 40px;">
                                        Ship My Milk
                                        <hr>
    							</td>
    						</tr>
    						<tr>
        						<td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #FFF; font-family: 'Postmates Std', 'Helvetica', -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif; font-size: 15px; font-smoothing: always; font-style: normal; font-weight: 400; letter-spacing: 0; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
                                    We're here to support you. Email us on <a href="mailto:support@smm.com"><b>support@smm.com</b></a> if you have any questions or concerns. You can also contact us at <b>407-605-0816</b>.
                                </td>
    						</tr>
    					</tbody>
    				</table>
    			</td>
    		</tr>
		</tbody>
	</table>
</body>
</html>
