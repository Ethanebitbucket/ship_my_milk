<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Reset Password | CTBF Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <body>
        <div style="width: 800px;margin: 0 auto;font-family: arial;font-size: 14px;">
            <table style="width: 100%;border-top: 0;font-family: arial;font-size: 14px;background: #212121;" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td></td>
                        <td style="text-align: center;padding: 10px;">
                            <img src="{{url('img/ctbf-logo.png')}}" width="230" height="40" alt=""/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%;border-top: 0;font-family: arial;font-size: 14px;" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="padding: 0 10px;border-left: 1px solid #ddd;border-right: 1px solid #ddd;">
                            <p style="margin:15px 0; padding: 0 0 60px 0;margin: 10px 0;"><span style="display: inline-block;float: left;margin-top: 20px; font-size: 18px;">Dear <?php echo $name; ?>,</span> </p>

                            <p style="margin:15px 0; font-family: 14px;color:green;"> Please click on the below link to reset your password. <a href="{{$recovery_url}}">Click Here</a> </p>


                            <p style="margin:15px 0; font-family: 14px;"> </p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%;font-family: arial;font-size: 14px;" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="padding: 10px;border-left: 1px solid #ddd;border-right: 1px solid #ddd;">
                            <p style="margin:15px 0; font-size: 16px;margin-bottom: 0;">Thank You, </p>
                            <p style="margin:15px 0; font-size: 16px;margin-top: 6px;">Choose To Be Fit</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%;border-top: 0;font-family: arial;font-size: 14px;background: #212121;" border="0" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td style="color: #ddd;"><p style="margin:15px 0; text-align: center;font-size: 12px;">� 2018 Property Portal - Powered by <a style="color: #d38235;text-decoration: none;" href="http://www.ethanetechnologies.com/" target="_blank">Ethane Web Technologies</a></p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>