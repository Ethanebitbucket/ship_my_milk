<?php
    return [
        'user_kids_msg' => "User's kids Details.",
        'kids_user_not_exist_msg' => "User doesn't exist!",
        'kids_not_save_msg' => "Something Went Wrong to save kid",
        'kids_save_msg' => "Kid Saved Successfully",
        'kids_found_msg' => "Kid Details",
        'kids_not_exist_msg' => "Kid is not exist",
        'kid_deleted_msg' => "Kid Successfully deleted",
        'kids_not_deleted_msg' => "Something Went Wrong to delete kid",
    ];
?>
