<?php

return [
    'welcome' => 'Welcome to our application',
    'method_success' => 'Communication method setting saved',
    'notification_success' => 'Notification setting saved',
    'notification_error' => 'Notification setting is not saved',
    'method_error' => 'Notification setting is not saved.',
    'error_msg' => 'Something went wrong.'
];
?>
