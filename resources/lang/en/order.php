<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'user_not_exist_msg' => "User doesn't exist!",
    'no_order_exist' => "No Order Found",
    'order_history' => "Order History",
    'rating_msg_success' => "Rating successfull submitted",
    'rating_msg_error' => "Something went wrong to save method",
    'order_status' => "order status updated",
];
