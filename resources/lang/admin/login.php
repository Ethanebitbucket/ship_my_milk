<?php 

return [

    'page_title'    =>  'Login',
    'register'      =>  'Register an account',
    'forget'        =>  'Forgot Password',
    'remember'      =>  'Remember Password',
];