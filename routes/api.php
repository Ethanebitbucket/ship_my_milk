<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\AuthController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */


Route::group(['prefix' => 'auth'], function () {
	Route::post('fedex-service-rate', 'Api\FedexController@get_service_rate');
	Route::post('get-tracking-number', 'Api\FedexController@get_tracking_number');
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');
    Route::get('unathorised', 'Api\AuthController@unathorised');
    Route::get('signup/activate', 'Api\AuthController@signupActivate');
    //change change email from profile
    Route::get('signup/reactivate', 'Api\AuthController@emailReactivate');
    Route::post('request-new-password', 'Api\AuthController@request_new_password');
});

Route::group(['middleware' => 'auth:api'], function() {

    Route::group(['prefix' => 'auth'], function() {
        //Braintree
        Route::get('get-braintree-token', 'Api\PaymentIntegrationController@brainTreeGetToken');
        Route::post('get-braintree-purchase-url', 'Api\PaymentIntegrationController@brainTreePurchaseUrl');
        Route::post('logout', 'Api\AuthController@logout');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('user-details', 'Api\UserController@user_details');
        Route::post('edit-user', 'Api\UserController@edit_user');
        Route::post('change-profile-image', 'Api\UserController@change_profile_image');
        Route::get('all_kid_details', 'Api\KidController@index');
        Route::delete('kid_delete/{kid_id}', 'Api\KidController@destroy')->where('kid_id', '[0-9]+');
        Route::get('kid_detail/{kid_id}', 'Api\KidController@show')->where('kid_id', '[0-9]+');
        Route::post('kid_add', 'Api\KidController@store');
        Route::post('kid_update', 'Api\KidController@update');
        Route::post('changepassword', 'Api\UserController@changePassword');
        Route::get('orderhistory', 'Api\OrderController@OrderHistory');
        Route::post('orderstatus', 'Api\OrderController@checkOrderStatus');
    });
    Route::get('home_details', 'Api\HomeController@index');
    Route::post('get_cart_price', 'Api\HomeController@get_cart_price');
    Route::post('save_order', 'Api\OrderController@saveOrder');
    Route::post('sendmail', 'Api\InformationController@contact');
    Route::post('rateservice', 'Api\OrderController@OrderRating');
    Route::post('communication_setting', 'Api\SettingController@CommunitcationMethod');
    Route::post('notification_setting', 'Api\SettingController@SwitchNotification');
});
Route::get('jsonfile', 'Api\GeographicController@genearteJsonFile');
Route::get('getjsonfile', 'Api\GeographicController@shareApiGeoJsonFile');
Route::get('about', 'Api\InformationController@about');
Route::get('tnc', 'Api\InformationController@tNc');
Route::get('privacy', 'Api\InformationController@Privacypolicy');
Route::any('/', function() {
    return response()->json([
            'app' => 'SHIP MY MILK',
            'version' => '1.0',
            'developed_by' => 'Ethane Web Technologies Pvt. Ltd.'
            ], 200);
});

// ALWAYS AT THE END OF THE PAGE
Route::fallback(function() {
    return response()->json([
            'message' => 'Page Not Found',
            'error' => 'If error persists, contact '
            ], 404);
});
