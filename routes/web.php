<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// IF ADMIN LOGGED OUT-------->

Route::group(['middleware'=>'IsAdminLoggedOut'], function(){

    Route::group(['prefix'=>'admin'], function(){
        Route::any('login', ['uses'=>'Admin\AdminController@login']);
    });

});



// IF ADMIN LOGGED IN -------->

Route::group(['middleware' => 'IsAdmin'], function(){
    Route::group(['prefix'=>'home'], function(){
        Route::get('dashboard', ['uses'=>'Admin\DashboardController@home']);
        Route::get('logout', ['uses'=>'Admin\DashboardController@logout']);
        Route::get('video-list', ['uses'=>'Admin\DashboardController@listing']);
        Route::get('change-status/{id}/{status}', 'Admin\DashboardController@changevideostatus');
        Route::any('update-video/{id}', 'Admin\DashboardController@updatevideo');
        Route::post('updatevideo', 'Admin\DashboardController@callupdate');
        Route::get('country-list', 'Admin\CountryController@countrylist');
        Route::get('box',          'Admin\BoxController@main');
        Route::get('services',     'Admin\BoxController@services');
        Route::get('contacts',     'Admin\BoxController@contacts');
        Route::get('information',     'Admin\BoxController@information');
        Route::any('information-update/{id}',     'Admin\BoxController@informationupdate');
        Route::any('box-update/{id}',     'Admin\BoxController@boxupdate');
        Route::any('box-info/{id}',     'Admin\BoxController@boxinfo');
        Route::any('admin',     'Admin\BoxController@adminupdate');
        Route::get('order',     'Admin\BoxController@userorder');
        Route::any('update-service/{id}',     'Admin\BoxController@updateservice');
    });

});